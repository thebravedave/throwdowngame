﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
/*
 These are the objects that are used in the RestService class when deserializing
 the JSON data objects.
 */
namespace throwdown
{

    public class ReturnObj
    {
        public bool success { get; set; }
        public JObject data { get; set; }
    }

    public class ReturnArrayObj
    {
        public bool success { get; set; }
        public List<JObject> data { get; set; }
    }

    public class Added
    {
        public int id { get; set; }
    }

    public class UserThrowAddDetails
    {
        public float percentile { get; set; }
        public Added leaderBoardAdded { get; set; }
        public Added userThrowAdded { get; set; }

    }
    public class Leaderboard
    {
        public int id { get; set; }
        public int userId { get; set; }
        public int score { get; set; }
        public DateTime throwDate { get; set; }
        public int throwDuration { get; set; }
    }
    public class UserThrow
    {
        public int id { get; set; }
        public int userId { get; set; }
        public int score { get; set; }
        public DateTime throwDate { get; set; }
        public int throwDuration { get; set; }
    }
    public class User
    {
        public int id { get; set; }
        public string userName { get; set; }
        public DateTime joinDate { get; set; }
        public string country { get; set; }

    }

    public class Updated
    {
        public int numberUpdated { get; set; }
        public int id { get; set; }

    }

    public class LeaderBoardUserContainer
    {
        public User user { get; set; }
        public Leaderboard leaderboard { get; set; }

        public LeaderBoardUserContainer(User us, Leaderboard lead)
        {
            user = us;
            leaderboard = lead;
        }
    }

}