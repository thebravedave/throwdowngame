﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;

/* 
 The RestService class is used for both the iOS and android code. It's methods are 
 used to communicate with the web service to send and recieve information from the 
 server. The methods in this class have the ability to register a user, update a 
 user's username, add a new score, get a user's throw scores, get a user's country's leadboard
 scores and the world leaderboard scores. 
 */

namespace throwdown
{
    public class RestService
    {
        // register a new user 
        public static Added RegistrationUsernameCountry(string username, string country)
        {
            // try to access webservice if error throw Exception
            try
            {
                // no trailing white space
                username = username.Trim();
                country = country.Trim();
                // use HttpClient communicate with web service
                HttpClient client = new HttpClient();
                // appropriate RESTful endpoint
                var RestUrl = "http://binarywebdesignllc.webhop.org/projects/throwdownbackend/webservices/rest.php/user/account/registration/" + username + "/" + country;
                var uri = new Uri(string.Format(RestUrl, string.Empty));
                ReturnObj item = null;
                Added aID = null;
                var response = client.GetAsync(uri).Result;
                // if successful communication with webservice deserialize JSON object
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    item = JsonConvert.DeserializeObject<ReturnObj>(content);
                    // webservice contained no information return null
                    if (item == null)
                    {
                        return null;
                    }
                    // successful call 
                    if (item.data != null)
                    {
                        aID = JsonConvert.DeserializeObject<Added>(item.data.ToString());
                    }
                }
                return aID;
            }
            catch (Exception ex)
            {
                Console.WriteLine("System timed out from webservice call: " + ex.Message);
                return null;
            }
        }

        // allows user to update username and country given a user id
        public static Updated UpdateUseridCountry(string id, string country, string username)
        {
            // try to access webservice if error throw Exception
            try
            {
                // no trailing white space
                username = username.Trim();
                country = country.Trim();
                // use HttpClient communicate with web service
                HttpClient client = new HttpClient();
                // appropriate RESTful endpoint
                var RestUrl = "http://binarywebdesignllc.webhop.org/projects/throwdownbackend/webservices/rest.php/user/account/update/" + id + "/" + country + "/" + username;
                var uri = new Uri(string.Format(RestUrl, string.Empty));
                ReturnObj item = null;
                Updated upd = null;
                var response = client.GetAsync(uri).Result;
                // if successful communication with webservice deserialize JSON object
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    item = JsonConvert.DeserializeObject<ReturnObj>(content);
                    if (item == null)
                    {
                        return null;
                    }
                    if (item.data != null)
                    {
                        upd = JsonConvert.DeserializeObject<Updated>(item.data.ToString());
                    }
                }
                return upd;
            }
            catch (Exception ex)
            {
                Console.WriteLine("System timed out from webservice call: " + ex.Message);
                return null;
            }

        }

        // Add a score value and duration for a given user id
        public static UserThrowAddDetails AddScore(string userId, string score, string throwDuration)
        {
            // try to access webservice if error throw Exception
            try
            {
                // use HttpClient communicate with web service
                HttpClient client = new HttpClient();
                // appropriate RESTful endpoint
                var RestUrl = "http://binarywebdesignllc.webhop.org/projects/throwdownbackend/webservices/rest.php/user/throw/add/" + userId + "/" + score + "/" + throwDuration;
                var uri = new Uri(string.Format(RestUrl, string.Empty));
                ReturnObj item = null;
                UserThrowAddDetails addDet = null;

                var response = client.GetAsync(uri).Result;
                // if successful communication with webservice deserialize JSON object
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    item = JsonConvert.DeserializeObject<ReturnObj>(content);
                    if (item == null)
                    {
                        return null;
                    }
                    if (item.data != null)
                    {
                        addDet = JsonConvert.DeserializeObject<UserThrowAddDetails>(item.data.ToString());
                    }
                }
                // contains information about the success of the throw and whether it was added to server
                return addDet;
            }
            catch (Exception ex)
            {
                Console.WriteLine("System timed out from webservice call: " + ex.Message);
                return null;
            }
        }



        // get user throws based on a user id - up to 10 throws saved on server
        public static List<UserThrow> GetUserThrows(string userId)
        {
            // try to access webservice if error throw Exception
            try
            {
                // use HttpClient communicate with web service
                HttpClient client = new HttpClient();
                // appropriate RESTful endpoint
                var RestUrl = "http://binarywebdesignllc.webhop.org/projects/throwdownbackend/webservices/rest.php/user/throws/" + userId;
                var uri = new Uri(string.Format(RestUrl, string.Empty));
                ReturnArrayObj item = null;
                List<UserThrow> userThrowList = new List<UserThrow>();

                var response = client.GetAsync(uri).Result;
                // if successful communication with webservice deserialize JSON object
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    item = JsonConvert.DeserializeObject<ReturnArrayObj>(content);
                    if (item == null)
                    {
                        return null;
                    }
                    if (item.data != null)
                    {
                        // iterate through json object deserializing each one
                        for (int i = 0; i < item.data.Count; i++)
                        {
                            try
                            {
                                UserThrow userThrow = JsonConvert.DeserializeObject<UserThrow>(item.data[i].ToString());

                                userThrowList.Add(userThrow);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }

                        }
                    }
                }
                return userThrowList;
            }
            catch (Exception ex)
            {
                Console.WriteLine("System timed out from webservice call: " + ex.Message);
                return null;
            }


        }

        // get the world leaderboard - up to 50 enteries saved on server
        public static List<LeaderBoardUserContainer> GetWorldLeaderBoard()
        {
            // try to access webservice if error throw Exception
            try
            {
                // use HttpClient communicate with web service
                HttpClient client = new HttpClient();
                // appropriate RESTful endpoint
                var RestUrl = "http://binarywebdesignllc.webhop.org/projects/throwdownbackend/webservices/rest.php/leaderboard/world";
                var uri = new Uri(string.Format(RestUrl, string.Empty));
                ReturnArrayObj item = null;
                List<LeaderBoardUserContainer> ulContainer = new List<LeaderBoardUserContainer>();

                var response = client.GetAsync(uri).Result;
                // if successful communication with webservice deserialize JSON object
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    LeaderBoardUserContainer lbc;
                    item = JsonConvert.DeserializeObject<ReturnArrayObj>(content);
                    if (item == null)
                    {
                        return null;
                    }
                    if (item.data != null)
                    {
                        // iterate through json object deserializing each one
                        for (int i = 0; i < item.data.Count; i++)
                        {
                            try
                            {
                                User user = JsonConvert.DeserializeObject<User>(item.data[i].GetValue("user").ToString());
                                Leaderboard leadboard = JsonConvert.DeserializeObject<Leaderboard>(item.data[i].GetValue("leaderboard").ToString());
                                lbc = new LeaderBoardUserContainer(user, leadboard);
                                ulContainer.Add(lbc);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }

                        }
                    }
                }
                return ulContainer;
            }
            catch (Exception ex)
            {
                Console.WriteLine("System timed out from webservice call: " + ex.Message);
                return null;
            }


        }

        // get leaderboard for a inputed country - up to 50 enteries saved
        public static List<LeaderBoardUserContainer> GetCountryLeaderBoard(string country)
        {
            // try to access webservice if error throw Exception
            try
            {
                // no trailing white space
                country = country.Trim();
                // use HttpClient communicate with web service
                HttpClient client = new HttpClient();
                // appropriate RESTful endpoint
                var RestUrl = "http://binarywebdesignllc.webhop.org/projects/throwdownbackend/webservices/rest.php/leaderboard/country/" + country;
                var uri = new Uri(string.Format(RestUrl, string.Empty));
                ReturnArrayObj item = null;
                List<LeaderBoardUserContainer> ulContainer = new List<LeaderBoardUserContainer>();

                var response = client.GetAsync(uri).Result;
                // if successful communication with webservice deserialize JSON object
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine(content);
                    LeaderBoardUserContainer lbc;
                    item = JsonConvert.DeserializeObject<ReturnArrayObj>(content);
                    if (item == null)
                    {
                        return null;
                    }
                    if (item.data != null)
                    {
                        // iterate through json object deserializing each one
                        for (int i = 0; i < item.data.Count; i++)
                        {
                            try
                            {
                                User user = JsonConvert.DeserializeObject<User>(item.data[i].GetValue("user").ToString());
                                Leaderboard leadboard = JsonConvert.DeserializeObject<Leaderboard>(item.data[i].GetValue("leaderboard").ToString());
                                lbc = new LeaderBoardUserContainer(user, leadboard);
                                ulContainer.Add(lbc);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }

                        }
                    }
                }
                return ulContainer;
            }
            catch (Exception ex)
            {
                Console.WriteLine("System timed out from webservice call: " + ex.Message);
                return null;
            }


        }

    }
}