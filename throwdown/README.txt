﻿This folder contains the shared code part of "Throwdown", a phone game where the goal is to throw your phone as hard as 
possible to maximize your score. The shared code holds the logic that is needed to make the calls to the
RESTful webservice which runs on an apache server. The primary two files in the folder are "DataTransferObjects.cs" which
contains the classes that the JSON objects deserialize to and "AccessWebService.cs" which contains the methods
that communicate to the web service. 

Description of the shared iOS and Android methods
------------------------------------------------
Method                          Description 
 
RegistrationUsernameCountry     Completes registration process for user, or retrns an error if username is already taken.   
UpdateUseridCountry             Allows user to update their country ID. 
AddScore                        Adds user's score to database upon successful completion of throw.  
GetUserThrows                   Returns history of user's throws. 
GetCountryLeaderBoard           Returns history of scores for user's current country.  
GetWorldLeaderBoard             Returns history of all scores in database.  