﻿using System;
using System.Globalization;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;

/*
 This controller/view is responsible for forcing the user to register when they use the app for the first time.
 The user has to create a username that will tie them to the server side leaderboard.  The controller also
 gets their country from the Android OS.  This username/country data is sent to the server and persisted in a
 database.
 */
namespace throwdown.Droid.Activities
{
    //this next line loads the view's title and locks the screen orientation into portrait mode
    [Activity(Label = "Register Username", ScreenOrientation = ScreenOrientation.Portrait)]
    public class RegisterUsername : Activity
    {
        //These next 4 lines load the controller widgets for submitting their username, username text, checkbox for agreeing to terms and conditions and the 
        //image that is clickable that shows the terms and conditions
        Button submitButton;
        EditText usernameEditText;
        CheckBox agreeToTermsAndConditionsCheckbox;
        ImageView showTermsAndConditionsImageView;

        //This is our modal which will hold the terms and conditions
        AlertDialog.Builder alert;

        //called when the controller is created
        protected override void OnCreate(Bundle savedInstanceState)
        {
            //first create base class constructor
            base.OnCreate(savedInstanceState);
            //set what view should go with this controller
            SetContentView(Resource.Layout.RegisterUsername);

            //These next 4 lines set our controller widgets to their corresponding view widgets
            submitButton = (Button)FindViewById(Resource.Id.submitbutton);
            usernameEditText = (EditText)FindViewById(Resource.Id.usernameedittext);
            agreeToTermsAndConditionsCheckbox = (CheckBox)FindViewById(Resource.Id.termsAndConditionsCheckbox);
            showTermsAndConditionsImageView = (ImageView)FindViewById(Resource.Id.termsAndConditionsImageView);
            //this sets our event handler for our terms and conditions question mark image
            showTermsAndConditionsImageView.Click += showTermsAndConditionsImageViewClicked;
            //this sets our event handler for our submit button for submiting the username
            submitButton.Click += SubmitButtonClicked;
            //this subroutine sets our modal to have the correct text information pertaining to the terms and conditions
            SetUpTermsAndConditions();

        }
        //this is our event handler for our question mark image which when clicked shows the terms and conditions modal
        private void showTermsAndConditionsImageViewClicked(object sender, EventArgs e)
        {
            //open up our terms and conditions modal
            RunOnUiThread(() => {
                alert.Show();
            });
        }
        //this is our event handler for when the submit button is clicked.  We need to make sure that they have agreed to terms and conditions
        //and their username/country is sent to the server
        private void SubmitButtonClicked(object sender, EventArgs e)
        {
            //determine if terms and condtions checkbox is checked
            if (agreeToTermsAndConditionsCheckbox.Checked == false)
            {
                Toast.MakeText(this, "You must agree to the terms and conditions to continue", ToastLength.Long).Show();
                return;
            }
            //get the username from the username text widget
            string username = usernameEditText.Text;
            //check to see if the username is empty
            if (username.Equals(""))
            {
                Toast.MakeText(this, "You have to enter a username", ToastLength.Long).Show();                        
            }

            //Get their country from the Android OS
            var globalization = RegionInfo.CurrentRegion;
            string threeLetterCountryCode = globalization.ThreeLetterISORegionName;
            string countryNativeName = globalization.NativeName;

            //send their username and country to the server. The server will register this to their phone if the username was not taken
            Added added = RestService.RegistrationUsernameCountry(username, countryNativeName);
            //check to see if the returned "Added" DTO is null, if so the server is down right now.
            if(added == null){
                Toast.MakeText(this, "The registration server is currently down or you are not connected to the internet, please try again.", ToastLength.Long).Show();            
            }
            else if(added != null && added.id != 0)
            {
                //if the added DTO is not null and the id in the added DTO is not zero then the username/country was registered with
                //the server.  We need to persist their username and country information with the app settings
                ISharedPreferences prefs = Application.Context.GetSharedPreferences("APPLICATION_PREFERENCES", FileCreationMode.Private);
                ISharedPreferencesEditor editor = prefs.Edit();
                editor.PutString("username", username);
                editor.PutString("country_three_letter_country_code", threeLetterCountryCode);
                editor.PutString("country_native_name", countryNativeName);
                editor.PutInt("user_id", added.id);
                editor.Apply();

                //now take them to the introduction1 controller/view
                StartActivity(typeof(Introduction1Activity));
            }
            else
            {
                //else the added DTO has an id that is zero which means their username was taken. Alert them of this fact
                Toast.MakeText(this, "The username you chose is taken.  Please try another.", ToastLength.Long).Show();
            }
        }

        private void SetUpTermsAndConditions()
        {

            //set modal up to be used with this controller/view
            alert = new AlertDialog.Builder(this);
            //set the title of the modal
            alert.SetTitle("Terms and Conditions");
            //set the message to appear in the view.
            string message = @"
1. Introduction
Before using the app, users of Throwdown must agree to the following 
terms and conditions. By using Throwdown, users must fully read these
terms and conditions and accept that they will comply with them

2. Permissions
When Throwdown is open, we may collect input from your phone including
accelerometer data, stored files and the phone's camera. If you do not
want Throwdown to use your phone's camera you may choose to disable the
camera in the app's settings.  

3. Privacy
Throwdown may collect information from you and publish it publicly.
This information may include a user-specified username, your phone's 
native locality, game scores and video of throws.

4. Risk of Damage
By using Throwdown you agree to accept responsibility for any damage
you cause to your phone, yourself, or anything else while you play 
Throwdown. The creators of Throwdown are not responsible for your 
actions when playing the game. Understand that throwing, dropping,
or shaking your phone may cause damage. 

5. Appropriate Usernames
Users must choose usernames that do not contain hate speech or obscenity.
The creators of Throwdown may remove from the leaderboards the 
scores of any users whose usernames we find inappropriate. However,
the creators of Throwdown should not be held responsible for any 
offensive speech that users submit to the leaderboards.  ";
            //set the message of the modal
            alert.SetMessage(message);
            //set the close button of the modal
            alert.SetNegativeButton("Close", (senderAlert, args) => {});

        }

    }
}
