﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;

/*
 This controller/view is responsible for showing the user's profile, which includes their
 username and their country.  From here a user can edit their profile.
 */
namespace throwdown.Droid.Activities
{
    //this next line loads the view's title and locks the screen orientation into portrait mode
    [Activity(Label = "", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ProfileActivity : Activity
    {
        //the next 3 lines load our username, and country text views and our "edit profile" button for our controller
        TextView userNameTextView;
        TextView countryTextView;
        Button editProfileButton;

        //called when the controller is created
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //set what view should go with this controller
            SetContentView(Resource.Layout.Profile);

            //These next 3 lines set our controller widgets to their corresponding view widgets
            userNameTextView = (TextView)FindViewById(Resource.Id.profileUserName);
            countryTextView = (TextView)FindViewById(Resource.Id.profileCountry);
            editProfileButton = (Button)FindViewById(Resource.Id.profileEditButton);
            //here we set the event handler for our edit profile button
            editProfileButton.Click += editProfileButtonClicked;
            //this subroutine loads our username and country for the controller/view
            LoadUserName();
        }
        //this is our event handler for our edit profile button.  This will start our edit profile controller/view
        private void editProfileButtonClicked(object sender, EventArgs e)
        {
            //here we load our edit profile controller/view
            StartActivity(typeof(EditProfileActivity));
        }
        //this method loads our username/country text widgets with the users country and username persisted in our app settings
        private void LoadUserName()
        {
            //ISharedPreferences holds our persisted app settings
            ISharedPreferences prefs = Application.Context.GetSharedPreferences("APPLICATION_PREFERENCES", FileCreationMode.Private);
            //get our username and country that have been persisted in app settings
            string username = prefs.GetString("username", null);
            string country = prefs.GetString("country_native_name", null);
            //set our text widgets with username and country 
            userNameTextView.Text = username;
            countryTextView.Text = country;                        
        }
        //This method creates our main menu at the top of our view
        public override bool OnCreateOptionsMenu(IMenu menu)  
        {  
            // set the menu layout  
            MenuInflater.Inflate(Resource.Menu.mainMenu, menu);  
            return base.OnCreateOptionsMenu(menu);  
        }
        //this is our event handler for our main menu's menu item being clicked
        public override bool OnOptionsItemSelected(IMenuItem item)  
        {  
            //check id of menu item clicked
            switch (item.ItemId)  
            {  
                //if correct menu item peform action
                case Resource.Id.menuItem1:  
                {  
                    //start up the menu controller/view
                    var throwResultsActivity = new Intent(this, typeof(MenuActivity));
                    StartActivity(throwResultsActivity);
                    return true;  
                }  
            }    
            return base.OnOptionsItemSelected(item);  
        } 
    }
}
