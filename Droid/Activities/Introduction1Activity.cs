﻿using System;
using Android.App;
using Android.Content.PM;
using Android.Hardware;
using Android.OS;
using Android.Runtime;
using Android.Views;
/*
 This controller/view is responsible for showing the user the first introduction view that shows the user
 what the software is about after the user registers when they user the software for the first time
 */
namespace throwdown.Droid.Activities
{
    //here we set the view title and lock the orientation into portrait orientation
    [Activity(Label = "Introduction To Throwdown Part 1", ScreenOrientation = ScreenOrientation.Portrait)]
    public class Introduction1Activity : Activity, GestureDetector.IOnGestureListener, ISensorEventListener
    {
        //here we set our gestureDetector which will be responsible for listening to right swipes which will allow the user to go to the next introduction controller/view
        GestureDetector gestureDetector;
        const int SWIPE_DISTANCE_THRESHOLD = 100;
        const int SWIPE_VELOCITY_THRESHOLD = 100;

        //called when the controller is created
        protected override void OnCreate(Bundle savedInstanceState)
        {
            //here we request the window to not show the title (the title above is for programming purposes only)
            RequestWindowFeature(WindowFeatures.NoTitle);
            //first create base class constructor
            base.OnCreate(savedInstanceState);
            //set what view should go with this layout
            SetContentView(Resource.Layout.Introduction1);
            //instantiate our gestureDetector for right swiping 
            gestureDetector = new GestureDetector(this);
        }
        //This method is called whenever a user swipes in any direction on the phone
        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            //calculate the distance of the swipe in x and y coordinates
            float distanceX = e2.GetX() - e1.GetX();
            float distanceY = e2.GetY() - e1.GetY();
            //here we determine if the swipe was greater than our threshold
            if (Math.Abs(distanceX) > Math.Abs(distanceY) && Math.Abs(distanceX) > SWIPE_DISTANCE_THRESHOLD && Math.Abs(velocityX) > SWIPE_VELOCITY_THRESHOLD)
            {
                //if the distanceX is greater then zero then they swiped right, ignore operation
                if (distanceX > 0)
                {
                    OnSwipeRight(); 
                }
                else
                {
                    //then they swiped left so we should go to next introduction controller/view
                    OnSwipeLeft();
                }
                return true;
            }
            return false;
        }
        void OnSwipeLeft()
        {      
            //then the user wants to go to the next introduction controller/view    
            StartActivity(typeof(Introduction2Activity));
        }
        //then they swiped right, do nothing
        void OnSwipeRight() {}
        //Have to register the touch event with our gestureDetector in order for swipe events to be sent out to our event handlers
        public override bool OnTouchEvent(MotionEvent e)
        {
            gestureDetector.OnTouchEvent(e);
            return false;
        }
        //These next 7 methods are methods from our interface and we have to implement them even though we don't use them
        public bool OnDown(MotionEvent e) { return true; }
        public void OnLongPress(MotionEvent e) { }
        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) { return true; }
        public void OnShowPress(MotionEvent e) { }
        public bool OnSingleTapUp(MotionEvent e) { return true; }
        public void OnAccuracyChanged(Sensor sensor, [GeneratedEnum] SensorStatus accuracy){}
        public void OnSensorChanged(SensorEvent e){}
    }
}
