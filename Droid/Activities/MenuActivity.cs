﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

/*
 This is the controller/view that shows our custom picture based main menu controller/view.
 From here the user can go to any controller/view on the site that is not part of registration or introduction
 */
namespace throwdown.Droid.Activities
{
    //here we set the view title and lock the orientation into portrait orientation
    [Activity(Label = "Main Menu", ScreenOrientation = ScreenOrientation.Portrait)]
    public class MenuActivity : Activity
    {
        //Here we create our controller widgets that will hold the images for our menu that the user can click on as menu items
        ImageView profileImageView;
        ImageView leaderBoardImageView;
        ImageView settingsImageView;
        ImageView throwImageView;
      
        //called when the controller is created
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //set what view should go with this controller
            SetContentView(Resource.Layout.Menu);

            //These next 4 lines set our controller widgets to their corresponding view widgets
            profileImageView = (ImageView)FindViewById(Resource.Id.imageProfile);
            leaderBoardImageView = (ImageView)FindViewById(Resource.Id.imageLeaderBoard);
            settingsImageView = (ImageView)FindViewById(Resource.Id.imageSettings);
            throwImageView = (ImageView)FindViewById(Resource.Id.imageThrowPhone);

            //the next 4 lines add the event handlers to our image menu item's click events
            profileImageView.Click += ProfileImageViewClicked;
            leaderBoardImageView.Click += LeaderBoardImageViewClicked;
            settingsImageView.Click += SettingsImageViewClicked;
            throwImageView.Click += ThrowImageViewClicked;

        }
        //this is our event handler for our throw phone menu item click event 
        private void ThrowImageViewClicked(object sender, EventArgs e)
        {
            //start the throw phone controller/view
            StartActivity(typeof(ThrowActivity));
        }
        //this is our event handler for our settings menu item click event
        private void SettingsImageViewClicked(object sender, EventArgs e)
        {
            //start the settings controller/view
            StartActivity(typeof(SettingsActivity));
        }
        //this is our event handler for our leader board menu item click event
        private void LeaderBoardImageViewClicked(object sender, EventArgs e)
        {
            //start the leaderboard controller/view
            StartActivity(typeof(LeaderboardActivity));
        }
        //this is our event handler for our profile menu item click event
        private void ProfileImageViewClicked(object sender, EventArgs e)
        {
            //start the profile controller/view
            StartActivity(typeof(ProfileActivity));
        }
    }
}
