﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using throwdown.Droid.Fragments;

/*
 This controller/view is responsible for showing the "LeaderBoard" which shows the high ranking community scores
 based on the world (all countries), the users country (your country), and the users top 10 throws.
 This controller/view has a Leaderboard menu which allows the user to choose what leaderboard it would like.
 when the user chooses this controller creates an android frament which will have the tabular data for the 
 top ranking users.  A fragment is like a small reusable controller/view.
 */
namespace throwdown.Droid.Activities
{
    //this next line loads the view's title and locks the screen orientation into portrait mode
    [Activity(Label = "Throwdown Leaderboards", ScreenOrientation = ScreenOrientation.Portrait)]
    public class LeaderboardActivity : Activity
    {
        //this creates our 3 text views which we use as a Leaderboard menu items for world, country, and my scores leaderboards
        TextView worldLeaderBoardMenuItemTextView;
        TextView countryLeaderBoardMenuItemTextView;
        TextView myScoresLeaderBoardMenuItemTextView;

        //called when the controller is created
        protected override void OnCreate(Bundle savedInstanceState)
        {
            //first create base class constructor
            base.OnCreate(savedInstanceState);
            //set what view should go with this controller
            SetContentView(Resource.Layout.Leaderboard);

            //These next 3 lines set our controller widgets to their corresponding view widgets
            worldLeaderBoardMenuItemTextView = (TextView)FindViewById(Resource.Id.leaderBoardMenuItemWorld);
            countryLeaderBoardMenuItemTextView = (TextView)FindViewById(Resource.Id.leaderBoardMenuItemCountry);
            myScoresLeaderBoardMenuItemTextView = (TextView)FindViewById(Resource.Id.leaderBoardMenuItemMyScores);

            //here we set our event handlers for our 3 Leaderboard menu items
            worldLeaderBoardMenuItemTextView.Click += WorldLeaderBoardMenuItemClicked;
            countryLeaderBoardMenuItemTextView.Click += CountryLeaderBoardMenuItemClicked;
            myScoresLeaderBoardMenuItemTextView.Click += MyScoresLeaderBoardMenuItemClicked;

            //when the controller is first loaded we load the world leader board first as the default leader board to view
            loadWorldLeaderBoardFragment();
        }
        //this is the event handler for our "My Scores" leaderboard menu item click event
        private void MyScoresLeaderBoardMenuItemClicked(object sender, EventArgs e)
        {
            //call load my scores leader board fragment subroutine
            loadMyScoresLeaderBoardFragment();
        }
        //this is the event handler for our "Country" leaderboard menu item click event
        private void CountryLeaderBoardMenuItemClicked(object sender, EventArgs e)
        {
            //call load country leaderboard fragment subroutine
            loadCountryLeaderBoardFragment();
        }
        //This is the event handler for our "World" leaderboard menu item click event
        private void WorldLeaderBoardMenuItemClicked(object sender, EventArgs e)
        {
            //call load world leaderboard fragment subroutine 
            loadWorldLeaderBoardFragment();
        }
        public void loadWorldLeaderBoardFragment()
        {
            //create our WorldLeaderBoard fragment
            WorldLeaderBoardFragment worldLeaderBoardFragment = new WorldLeaderBoardFragment();
            //create a transaction which will control which fragment is shown below the Leaderboard menu
            FragmentTransaction fragmentTx = this.FragmentManager.BeginTransaction();
            //begin the fragment transaction
            fragmentTx = this.FragmentManager.BeginTransaction();
            //this next line replaces any fragment which has been loaded into our view with the world leaderboard fragment we are about to start
            fragmentTx.Replace(Resource.Id.leaderBoardFragmentContainer, worldLeaderBoardFragment);
            //committing our fragment transaction loads our fragment into our view
            fragmentTx.Commit();
        }
        public void loadCountryLeaderBoardFragment()
        {
            //create our country leaderboard fragment
            CountryLeaderBoardFragment countryLeaderBoardFragment = new CountryLeaderBoardFragment();
            //create a transaction which will control which fragment is shown below the Leaderboard menu
            FragmentTransaction fragmentTx = this.FragmentManager.BeginTransaction();
            //begin the fragment transaction
            fragmentTx = this.FragmentManager.BeginTransaction();
            //this next line replaces any fragment which has been loaded into our view with the world leaderboard fragment we are about to start
            fragmentTx.Replace(Resource.Id.leaderBoardFragmentContainer, countryLeaderBoardFragment);
            //committing our fragment transaction loads our fragment into our view
            fragmentTx.Commit();
        }
        public void loadMyScoresLeaderBoardFragment()
        {
            //create our my scores leaderboard fragment
            MyScoresFragment myScoresFragment = new MyScoresFragment();
            //create a transaction which will control which fragment is shown below the Leaderboard menu
            FragmentTransaction fragmentTx = this.FragmentManager.BeginTransaction();
            //begin the fragment transaction
            fragmentTx = this.FragmentManager.BeginTransaction();
            //this next line replaces any fragment which has been loaded into our view with the world leaderboard fragment we are about to start
            fragmentTx.Replace(Resource.Id.leaderBoardFragmentContainer, myScoresFragment);
            //committing our fragment transaction loads our fragment into our view
            fragmentTx.Commit();
        }
        //this creates our main menu at the top of the screen
        public override bool OnCreateOptionsMenu(IMenu menu)  
        {  
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mainMenu, menu);             
            return base.OnCreateOptionsMenu(menu);  
        }
        //this next method is called when a menu item from the main menu is clicked
        public override bool OnOptionsItemSelected(IMenuItem item)  
        {  
            //check the menu item's id
            switch (item.ItemId)  
            {  
                case Resource.Id.menuItem1:  
                {  
                    //Start the menu controller/view
                    var menuActivityIntent = new Intent(this, typeof(MenuActivity));
                    StartActivity(menuActivityIntent);
                    return true;  
                }  
            }  
            return base.OnOptionsItemSelected(item);  
        } 
    }
}
