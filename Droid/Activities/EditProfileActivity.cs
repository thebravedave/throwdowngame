﻿using System;
using System.Globalization;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;

/*
 This class is the controller/view that is responsible for allowing a user to edit their username and country.
 The username is updated through an editable EditText widget and the country is obtained from the Android OS
 */
namespace throwdown.Droid.Activities
{
    //Here is where we set the label for our view and lock the screen orientation
    [Activity(Label = "Edit Profile", ScreenOrientation = ScreenOrientation.Portrait)]
    public class EditProfileActivity : Activity
    {
        //here we set our widgets for our view
        EditText userNameEditText;
        TextView countryTextView;
        Button editProfileButton;
        //here we create our ISharedPreferences member variable which is how we get and set persisted app data
        ISharedPreferences prefs;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            //Set what view should go with this controller
            SetContentView(Resource.Layout.EditProfile);
            //next 3 lines tell the controller what controller member variable widgets should go to which view widgets
            userNameEditText = (EditText) FindViewById(Resource.Id.editProfileUserNameEditText);
            countryTextView = (TextView) FindViewById(Resource.Id.editProfileCountryTextView);
            editProfileButton = (Button) FindViewById(Resource.Id.editProfileButton);
            //here we set what action should occur when the user clicks the edit profile button
            editProfileButton.Click += EditProfileButtonClicked;

            //need to set user name text and country text with the current username
            //first get the user name and country from the app persisted data
            prefs = Application.Context.GetSharedPreferences("APPLICATION_PREFERENCES", FileCreationMode.Private);
            string username = prefs.GetString("username", null);
            string country = prefs.GetString("country_native_name", null);
            //set the username country widgets text values to be equal to the persisted username and country
            userNameEditText.Text = username;
            countryTextView.Text = country;
        }

        private void EditProfileButtonClicked(object sender, EventArgs e)
        {
            //need to send the username and country to the server to be updated
            //get the username from the current edit text widget
            string userName = userNameEditText.Text;
            int userId = prefs.GetInt("user_id", 0);

            //get the current country from the device.
            var globalization = RegionInfo.CurrentRegion;
            string threeLetterCountryCode = globalization.ThreeLetterISORegionName;
            string countryNativeName = globalization.NativeName;

            //send the username and country to the server to be updated
            Updated updated = RestService.UpdateUseridCountry(userId.ToString(), countryNativeName, userName);
            if (updated == null)
            {
                //if updated DTO is null the server is down, let the user know this
                Toast.MakeText(this, "The registration server is currently down or you are not connected to the internet, please try again.", ToastLength.Long).Show();            

            }
            else if (updated.id == 0)
            {
                //if the updated id is zero then the update did not occur.  This would be because the username is taken
                string errorMessage = "The username:" + userName + " is taken.  Please choose another.";
                Toast.MakeText(this, errorMessage, ToastLength.Long).Show();
            }
            else
            {
                //else the change on the server was successful and we have to save the username and country to the app persisted data
                ISharedPreferencesEditor editor = prefs.Edit();
                editor.PutString("username", userName);
                editor.PutString("country_three_letter_country_code", threeLetterCountryCode);
                editor.PutString("country_native_name", countryNativeName);
                editor.Apply();
                //this was successful so take them back to the Profile controller/view
                StartActivity(typeof(ProfileActivity));
            }
        }
        //this method creates our main top menu for us
        public override bool OnCreateOptionsMenu(IMenu menu)  
        {  
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mainMenu, menu);  
            return base.OnCreateOptionsMenu(menu);  
        }
        //this method is called when a menu item from our main top menu is clicked.
        public override bool OnOptionsItemSelected(IMenuItem item)  
        {  
            //get id of the clicked menu item
            switch (item.ItemId)  
            {  
                //if the clicked menu item was our menu item then start the menu controller/view
                case Resource.Id.menuItem1:  
                {  
                    //start up the menu controller/view
                    var menuActivity = new Intent(this, typeof(MenuActivity));
                    StartActivity(menuActivity);
                    return true;  
                }  
            }   
            return base.OnOptionsItemSelected(item);  
        } 
    }
}
