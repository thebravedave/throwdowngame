﻿using System;
using Android.App;
using Android.Content.PM;
using Android.Hardware;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

/*
 This controller/view is responsible for showing the user the third introduction view that shows the user
 what the software is about after the user registers when they user the software for the first time
 */
namespace throwdown.Droid.Activities
{
    //here we set the view title and lock the orientation into portrait orientation
    [Activity(Label = "Introduction to Throwdown Part 3", ScreenOrientation = ScreenOrientation.Portrait)]
    public class Introduction3Activity : Activity, GestureDetector.IOnGestureListener, ISensorEventListener
    {
        //here we set our gestureDetector which will be responsible for listening to right swipes which will allow the user to go to the next introduction controller/view
        GestureDetector gestureDetector;
        const int SWIPE_DISTANCE_THRESHOLD = 100;
        const int SWIPE_VELOCITY_THRESHOLD = 100;

        //here we create our controller playButton widget which when clicked will send the user to the throw your phone controller/view
        Button playButton;
        //called when the controller is created
        protected override void OnCreate(Bundle savedInstanceState)
        {
            //here we request the window to not show the title (the title above is for programming purposes only)
            RequestWindowFeature(WindowFeatures.NoTitle);
            //first create base class constructor
            base.OnCreate(savedInstanceState);
            //set what view should go with this controller
            SetContentView(Resource.Layout.Introduction3);

            //set our controller button widget to it's corresponding view widget 
            playButton = (Button) FindViewById(Resource.Id.introduction3PlayButton);
            //set what should happend when a user clicks the "Play" button
            playButton.Click += PlayButtonClicked;
            //instantiate our gestureDetector for left/right swiping 
            gestureDetector = new GestureDetector(this);
        }
        //This is the even handler for our "play" button being clicked
        private void PlayButtonClicked(object sender, EventArgs e)
        {
            //start our throw phone controller/view
            StartActivity(typeof(ThrowActivity));
        }

        //This method is called whenever a user swipes in any direction on the phone
        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            //calculate the distance of the swipe in x and y coordinates
            float distanceX = e2.GetX() - e1.GetX();
            float distanceY = e2.GetY() - e1.GetY();
            //here we determine if the swipe was greater than our threshold
            if (Math.Abs(distanceX) > Math.Abs(distanceY) && Math.Abs(distanceX) > SWIPE_DISTANCE_THRESHOLD && Math.Abs(velocityX) > SWIPE_VELOCITY_THRESHOLD)
            {
                //if the distanceX is greater then zero then they swiped right and we go to previous controller/view
                if (distanceX > 0)
                {
                    OnSwipeRight();
                }
                else
                {
                    //then they swiped left so we ignore this operation
                    OnSwipeLeft();
                }
                return true;
            }
            return false;
        }
        //We ignore swipe left because we have them click the start button to begin the game
        void OnSwipeLeft(){}
        //then they swiped right, go to previous introduction controller/view
        void OnSwipeRight()
        {
            //start introduction 2 controller/view
            StartActivity(typeof(Introduction2Activity));
        }
        //Have to register the touch event with our gestureDetector in order for swipe events to be sent out to our event handlers
        public override bool OnTouchEvent(MotionEvent e)
        {
            gestureDetector.OnTouchEvent(e);
            return false;
        }
        //These next 7 methods are methods from our interface and we have to implement them even though we don't use them
        public bool OnDown(MotionEvent e) { return true; }
        public void OnLongPress(MotionEvent e) { }
        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) { return true; }
        public void OnShowPress(MotionEvent e) { }
        public bool OnSingleTapUp(MotionEvent e) { return true; }
        public void OnAccuracyChanged(Sensor sensor, [GeneratedEnum] SensorStatus accuracy) { }
        public void OnSensorChanged(SensorEvent e) { }
    }
}
