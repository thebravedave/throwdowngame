﻿using Android.App;
using Android.OS;
using Android.Content;
using Android.Content.PM;

/*
This is the first controller/view loaded.  It's main purpose is to determine if the user has used the application
yet.  If the user has then they will have registered a username which is persisted in the app settings.  In this
case we send them to the throw phone controller/view.  If they haven't used the app before we send them to the
registration controller/view
*/
namespace throwdown.Droid.Activities
{
    //here we set our controller to be the main launcher which is the controller loaded when the app starts.  We also lock the orientation in portrain mode here
    [Activity(Label = "Throwdown", MainLauncher = true, Icon = "@mipmap/icon", ScreenOrientation = ScreenOrientation.Portrait, Theme = "@android:style/Theme.NoTitleBar")]
    public class MainActivity : Activity
    {
        //called when the controller is created
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            //set what view should go with this controller
            SetContentView(Resource.Layout.Main);
            CheckWhereToSendUser();

        }
        //if the controller is started again after being paused
        protected override void OnResume()
        {
            base.OnResume();
            CheckWhereToSendUser();
        }
        private void CheckWhereToSendUser()
        {
            //ISharedPreferences is where we persist our data for our app
            ISharedPreferences prefs = Application.Context.GetSharedPreferences("APPLICATION_PREFERENCES", FileCreationMode.Private);

            //here we see if the username has been set yet, meaning the user has registered already
            string username = prefs.GetString("username", null);
            if (username == null)
            {
                //then the user has not started the app before and registered a username and country,
                //we should send them to the controller/view where they can register a username
                StartActivity(typeof(RegisterUsername));
            }
            else
            {
                //else the user has already registered a username and country and we can send them to the throw phone controller/view
                StartActivity(typeof(ThrowActivity));
            }
        }
    }
}

