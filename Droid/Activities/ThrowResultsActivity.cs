﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;

/*
 This controller/view is responsible for sending the user's throw score, and duration data to the server.
 If the user gets a high score the view shows that the user ranked in the leaderboard.
 The view for this controller also shows how they ranked (in a percentile) in comparison to all the throws 
 thrown by all users recorded on the backend
 */
namespace throwdown.Droid.Activities
{
    //here we set the view title and lock the orientation into portrait orientation
    [Activity(Label = "Thrown Phone Results", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ThrowResultsActivity : Activity
    {
        //the next 5 lines load our controllers widgets for score, duration, percentile and if they ranked in the leaderboard
        TextView maxSpeedTextView;
        TextView durationTextView;
        TextView topPercentile;
        ImageView topScoreImageView;
        Button throwAgainButton;

        //called when the controller is created
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            //set what view should go with this controller
            SetContentView(Resource.Layout.ThrowResult);

            //These next 5 lines set our controller widgets to their corresponding view widgets
            maxSpeedTextView = (TextView) FindViewById(Resource.Id.maxSpeed);
            durationTextView = (TextView)FindViewById(Resource.Id.duration);
            topPercentile = (TextView)FindViewById(Resource.Id.topPercentile);
            topScoreImageView = (ImageView)FindViewById(Resource.Id.throwResultNewTopScore);
            throwAgainButton = (Button)FindViewById(Resource.Id.throwAgainButton);
            //here we set the event handler for our "throw again" button
            throwAgainButton.Click += ThrowAgainButtonClicked;

            //get the maxSpeed(score) and duration sent from the the Throw controller/view
            string maxSpeed = Intent.GetStringExtra("maxSpeed");
            string duration = Intent.GetStringExtra("duration");

            //set the text for our score and duration text widgets
            maxSpeedTextView.Text = "Score: " + maxSpeed;
            durationTextView.Text = "Duration: " + duration + " milliseconds";

            //need to get their user id from the app persisted data
            ISharedPreferences prefs = Application.Context.GetSharedPreferences("APPLICATION_PREFERENCES", FileCreationMode.Private);
            int userId = prefs.GetInt("user_id", 0);

            //here we send the userid, score, and duration to the server
            UserThrowAddDetails userThrowAddDetails = RestService.AddScore(userId.ToString(),maxSpeed,duration);

            //if the userThrowsAddDetails DTO is null the server is down
            if(userThrowAddDetails == null)
            {
                Toast.MakeText(this, "The registration server is currently down or you are not connected to the internet, please try again.", ToastLength.Long).Show();            
            
            }
            if(userThrowAddDetails != null && userThrowAddDetails.userThrowAdded.id != 0)
            {
                //then the score was added and we should show their percentile
                topPercentile.Text = "Top Percentile: " + userThrowAddDetails.percentile.ToString() + "%";
            }
            if(userThrowAddDetails != null && userThrowAddDetails.leaderBoardAdded.id != 0)
            {
                //then the user ranked in the leaderboard and we should show our "new high score" image
                topScoreImageView.Visibility = ViewStates.Visible;
            }         
        }
        //here is our event handler for when the use clicks the "Throw Again" button
        private void ThrowAgainButtonClicked(object sender, EventArgs e)
        {
            //start the Throw Phone activity
            var throwActivity = new Intent(this, typeof(ThrowActivity));
            StartActivity(throwActivity); 
        }

        //this creates our main menu at the top of the view
        public override bool OnCreateOptionsMenu(IMenu menu)  
        {  
            // set the menu layout  
            MenuInflater.Inflate(Resource.Menu.mainMenu, menu);  
            return base.OnCreateOptionsMenu(menu);  
        }
        //this is our event handler for our main menu's menu item being clicked
        public override bool OnOptionsItemSelected(IMenuItem item)  
        {  
            //check id of menu item clicked
            switch (item.ItemId)  
            {  
                //if correct menu item peform action
                case Resource.Id.menuItem1:  
                {  
                    //start up the menu controller/view
                    var throwResultsActivity = new Intent(this, typeof(MenuActivity));
                    StartActivity(throwResultsActivity);
                    return true;  
                }  
            }  
  
            return base.OnOptionsItemSelected(item);  
        } 
    }
}
