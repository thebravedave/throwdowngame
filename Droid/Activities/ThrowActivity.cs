﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Hardware;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using throwdown.Droid.Helpers;

/*
 This controller/view is responsible for handling all the logic for when the user throws their phone to
 get scores based on how fast they throw their phone.  The faster they throw the phone, the higher their 
 score is.  To start the user swipes up and then throws their phone.  The user will also have video for
 the throw saved to their phone if they have that chosen in the settings page.
 */
namespace throwdown.Droid.Activities
{
    //this next line loads the view's title and locks the screen orientation into portrait mode
    [Activity(Label = "Throw Your Phone and Score!", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ThrowActivity : Activity, GestureDetector.IOnGestureListener, ISensorEventListener
    {
        //here we set our gestureDetector which will be responsible for listening to right swipes which will allow the user to start their throw session
        private GestureDetector _gestureDetector;

        //here we put a lock on our accelerometer sensor while we get the current accelerometer data
        static readonly object _syncLock = new object();
        //SensorManager holds our current accelerometer data.
        SensorManager _sensorManager;

        //we use this boolean to know when to start reading the acceleration data.
        bool readAcccelerationData = false;
        //this maxSpeed contains the highest acceleration point and we use it to determine the score
        float maxSpeed = 0.0f;

        //These next 4 lines load the controller widgets for showing what the current acceleration score is,
        //the video for saving video, and the VideoRecorderManager which is a custom class used to record the video
        TextView accelerationTextView;
        VideoView throwVideo;
        VideoRecorderManager videoRecordingManager;

        //The stopwatch will record the duration of the throw
        Stopwatch stopwatch;

        //called when the controller is created
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //set what view should go with this controller
            SetContentView(Resource.Layout.ThrowActivity);

            //this enstantiates a new VideoRecordingManager
            videoRecordingManager = new VideoRecorderManager();

            //These next 2 lines set our controller widgets to their corresponding view widgets
            accelerationTextView = (TextView) FindViewById(Resource.Id.accelerationTextView);
            throwVideo = (VideoView)FindViewById(Resource.Id.throwVideo);
   
            //this enstantiates a new GestureDetector which will be used for registering the swiping up which is used for starting the throw
            _gestureDetector = new GestureDetector(this);

            //this entantiates a new Stopwatch which will be used for determining throw duration
            stopwatch = new Stopwatch();

            //Here we load the sensor service into our local _sensorManager which holds current acceleration data and will send 
            //events to our sensor event handler defined below
            _sensorManager = (SensorManager)GetSystemService(Context.SensorService);
        }

        //This method creates our main menu at the top of our view
        public override bool OnCreateOptionsMenu(IMenu menu)  
        {  
            // set the menu layout 
            MenuInflater.Inflate(Resource.Menu.mainMenu, menu);  
            return base.OnCreateOptionsMenu(menu);  
        }
        //this is our event handler for our main menu's menu item being clicked
        public override bool OnOptionsItemSelected(IMenuItem item)  
        {  
            //check id of menu item clicked
            switch (item.ItemId)  
            {  
                //if correct menu item peform action
                case Resource.Id.menuItem1:  
                    {  
                        //start up the menu controller/view
                        var throwResultsActivity = new Intent(this, typeof(MenuActivity));
                        StartActivity(throwResultsActivity);
                        return true;  
                    }  
            }   
            return base.OnOptionsItemSelected(item);  
        } 
        //this event handler is called when the user swiped on their phone which means they are ready to throw their phone to get points
        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            //start recording video (checks if proper settings are set internall)
            videoRecordingManager.RecordVideo(throwVideo);
            //we need to start testing our accelerometer speed now
            readAcccelerationData = true;
            //start time duration calculation
            stopwatch.Start();
            return true;
        }
        //Have to register the touch event with our gestureDetector in order for swipe events to be sent out to our event handlers
        public override bool OnTouchEvent(MotionEvent e)
        {
            _gestureDetector.OnTouchEvent(e);
            return false;
        }
        //These next 5 methods are methods from our interface and we have to implement them even though we don't user them
        public void OnLongPress(MotionEvent e){}
        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) { return true; }
        public void OnShowPress(MotionEvent e){}
        public bool OnSingleTapUp(MotionEvent e) { return true; }
        public bool OnDown(MotionEvent e) { return true; }

        //this is called if the app is stoped then started again. If so we need to re register the sensor manager which controls acceleration
        protected override void OnResume()
        {
            base.OnResume();
            _sensorManager.RegisterListener(this, _sensorManager.GetDefaultSensor(SensorType.LinearAcceleration), SensorDelay.Ui);
        }
        //this is called if the app is paused.  If so we need to unregister our acceleration sensor listener
        protected override void OnPause()
        {
            base.OnPause();
            _sensorManager.UnregisterListener(this);
        }
        //this is a method needed to be implemented by our interface but we do not use it
        public void OnAccuracyChanged(Sensor sensor, [GeneratedEnum] SensorStatus accuracy){}

        //this is the event handler called whenever the acceleration hardware detects a change
        public void OnSensorChanged(SensorEvent e)
        {
            //lock our acceleration changed data so we can operate on it without it changing on us
            lock (_syncLock)
            {
                //get the x,y, and z acceration data
                float xAccel = Math.Abs((float)(e.Values[0]));
                float yAccel = Math.Abs((float)(e.Values[1]));
                float zAccel = Math.Abs((float)(e.Values[2]));

                //these next 3 if statments will set the maxSpeed to be the largest of the x,y, or z acceleration
                //data if any are larger than the current maxSpeed
                if(xAccel > maxSpeed){
                    maxSpeed = xAccel;
                }
                if(yAccel > maxSpeed){
                    maxSpeed = yAccel;
                }
                if(zAccel > maxSpeed){
                    maxSpeed = zAccel;
                }
                //we check if we are reading acceleration data yet (have they swiped up?)
                if (readAcccelerationData)
                {
                    //get floor of max speed
                    maxSpeed = (float)Math.Floor(maxSpeed);
                    //here we check if their throw is ended. We do this using a threshhold of x,y,z data and 
                    //a min acceleration maxSpeed.
                    if (xAccel < .3 && yAccel < .3 && zAccel < .3 && maxSpeed > 8)
                    {
                        //set readAccelerationData to false so we don't collect more acceleration data since their throw is complete
                        readAcccelerationData = false;
                        //stop calculating throw duration
                        stopwatch.Stop();
                        //Send notice that their throw is completed
                        Toast.MakeText(this, "throw complete!", ToastLength.Short).Show();

                        //send the maxSpeed and the duration in seconds to the ThrowResults controller/view
                        var throwResultsActivity = new Intent(this, typeof(ThrowResultsActivity));
                        TimeSpan ts = stopwatch.Elapsed;
                        int timeElapsedInSeconds = ts.Milliseconds;
                        //stop recording the video from their throw
                        videoRecordingManager.StopRecordingVideo(maxSpeed, timeElapsedInSeconds);

                        //pass the duration and maxSpeed to the ThrowResults controller/view
                        throwResultsActivity.PutExtra("duration", timeElapsedInSeconds.ToString());
                        throwResultsActivity.PutExtra("maxSpeed", maxSpeed.ToString());
                        //start up the throwresults controller/view
                        StartActivity(throwResultsActivity);
                    }

                }
                //here we check if the "score" text widget is not null and if we are reading acceleration data.
                //if so we set the "score" text widget to have the current highest acceleration value
                if(accelerationTextView != null && readAcccelerationData)
                {
                    accelerationTextView.Text = maxSpeed.ToString();

                }          
            }
        }
    }
}
