﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
/*
 This is the controller/view that is used as a settings page.  Here the user can choose whether they
 would like to allow the app to save video recordings of each of their throws.  The recordings are added
 to the users internal storage in /DCIM/Throwdown
 */
namespace throwdown.Droid.Activities
{
    //this next line loads the view's title and locks the screen orientation into portrait mode
    [Activity(Label = "Settings", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SettingsActivity : Activity
    {
        //here we create our controllers checkbox widget
        CheckBox saveVideoCheckBox;

        //the next 2 lines are used for accessing/saving persisted app data
        ISharedPreferencesEditor editor;
        ISharedPreferences prefs;

        //called when the controller is created
        protected override void OnCreate(Bundle savedInstanceState)
        {
            //first create base class constructor
            base.OnCreate(savedInstanceState);
            //set what view should go with this controller
            SetContentView(Resource.Layout.Settings);

            //This next line sets our controller widget to its corresponding view widget
            saveVideoCheckBox = (CheckBox) FindViewById(Resource.Id.settingsCollectVideoCheckBox);
            //here we set our event handler for our widget checkbox
            saveVideoCheckBox.Click += saveVideoCheckBoxClicked;

            //this loads our persisted app data 
            prefs = Application.Context.GetSharedPreferences("APPLICATION_PREFERENCES", FileCreationMode.Private);
            editor = prefs.Edit();

            //need to get their save video settings if they already exist
            bool saveVideoSettings = prefs.GetBoolean("save_video_settings", false);
            //set the checkbox to have the perisisted checkbox settings for saving videos
            saveVideoCheckBox.Checked = saveVideoSettings;
        }
        //this is our event handler for when the checkbox is checked or unchecked
        private void saveVideoCheckBoxClicked(object sender, EventArgs e)
        {
            //check to see if the checkbox is checked or unchecked
            if (saveVideoCheckBox.Checked)
            {
                //it was just checked so we need to persist the save video settings is true in the persisted app settings
                editor.PutBoolean("save_video_settings", true);
                editor.Apply();

            }
            else
            {
                //it was just unchecked so we need to persist the save video settings to false in the persisted app settings
                editor.Remove("save_video_settings");
                editor.Apply();
            }
        }
        //this creates our main menu at the top of the screen
        public override bool OnCreateOptionsMenu(IMenu menu)  
        {  
            // set the menu layout
            MenuInflater.Inflate(Resource.Menu.mainMenu, menu);  
            return base.OnCreateOptionsMenu(menu);  
        }
        //this next method is called when a menu item from the main menu is clicked
        public override bool OnOptionsItemSelected(IMenuItem item)  
        {  
            //check the menu item's id
            switch (item.ItemId)  
            {  
                case Resource.Id.menuItem1:  
                {  
                    //Start the menu controller/view
                    var throwResultsActivity = new Intent(this, typeof(MenuActivity));
                    StartActivity(throwResultsActivity);
                    return true;  
                }  
            }  
            return base.OnOptionsItemSelected(item);  
        } 

    }
}
