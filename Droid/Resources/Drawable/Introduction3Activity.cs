﻿using System;
using Android.App;
using Android.Hardware;
using Android.OS;
using Android.Runtime;
using Android.Views;

namespace throwdown.Droid.Activities
{
    [Activity(Label = "Introduction3Activity")]
    public class Introduction3Activity : Activity, GestureDetector.IOnGestureListener, ISensorEventListener
    {
        GestureDetector gestureDetector;
        const int SWIPE_DISTANCE_THRESHOLD = 100;
        const int SWIPE_VELOCITY_THRESHOLD = 100;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Introduction3);

            gestureDetector = new GestureDetector(this);
        }
        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            float distanceX = e2.GetX() - e1.GetX();
            float distanceY = e2.GetY() - e1.GetY();
            if (Math.Abs(distanceX) > Math.Abs(distanceY) && Math.Abs(distanceX) > SWIPE_DISTANCE_THRESHOLD && Math.Abs(velocityX) > SWIPE_VELOCITY_THRESHOLD)
            {
                if (distanceX > 0)
                    OnSwipeRight();
                else
                    OnSwipeLeft();
                return true;
            }
            return false;
        }
        void OnSwipeLeft(){}
        void OnSwipeRight()
        {
            StartActivity(typeof(Introduction2Activity));
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            gestureDetector.OnTouchEvent(e);
            return false;
        }
        public bool OnDown(MotionEvent e) { return true; }
        public void OnLongPress(MotionEvent e) { }
        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) { return true; }
        public void OnShowPress(MotionEvent e) { }
        public bool OnSingleTapUp(MotionEvent e) { return true; }
        public void OnAccuracyChanged(Sensor sensor, [GeneratedEnum] SensorStatus accuracy) { }
        public void OnSensorChanged(SensorEvent e) { }
    }
}
