﻿The Android part of this Xamarin project is the mobile development part of the "Throwdown" project,
which is a phone game where you throw your phone and based on the speed of the phone you get scores for high speeds.
The Android part has all of the phone controllers and views for the game.
The game consists of Activities and their views as well as Fragments and fragment views.
The Android part of the code also makes use of "shared code" which holds the logic that makes all of the calls to the
RESTful webservice for the app which is run on an apache server.


Activities used by the application
----------------------------------------
MainActivity - Activity loaded when starting the application. Sends user to RegisterUsername if they have not registered, otherwise sends them to the ThrowActivity.
RegisterUsername - Activity loaded during user registration
Introduction1Activity - Acts as the first introduction slide. Allows user to advance through other introduction slides
Introduction2Activity - Acts as the second introduction slide. Allows user to advance through other introduction slides
Introduction3Activity - Acts as the third introduction slide. Allows user to advance through other introduction slides
ThrowActivity - Acts as the primary functional activity for the application, allowing users to play the game and access the MenuActivity.
ThrowResultsActivity - Triggers as a result of successful completion of a ThrowActivity. Displays results of the user's throw
MenuActivity - Container for user to access ProfileActivity, LeaderboardActivity, and SettingsActivity. 
EditProfileActivity - Allows user to edit and save their profile information.
ProfileActivity - Allows user to view their profile information. Also allows users to access EditProfileActivity.
SettingsActivity - Allows user to edit local settings for their application.
LeaderboardActivity - Allows users to view local, national, and global scores.  Loads framents for country, global, as well as personal best user throws
