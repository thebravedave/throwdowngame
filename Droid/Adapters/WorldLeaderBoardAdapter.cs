﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Android.Content;
using Android.Views;
using Android.Widget;
using throwdown.Droid.Helpers;

/*
 This is the Country leader board adapter.  The country leaderboard adapter is responsible for creating the binding 
 of the controller/model's country leader data set (in list format) to the front end tabular list view.  This allows
 the tabular list view to be populated with the data from the country leader data set (in list format).
 */
namespace throwdown.Droid.Adapters
{
    public class WorldLeaderBoardAdapter : ArrayAdapter
    {

        //here is our context which is basically a reference to the Activity (controller) that called this fragment
        private Context c;
        //this is our leader data set
        private List<LeaderBoardUserContainer> leaders;
        //this is the LayoutInflator which is responsible for populating the tablular listview with rows
        private LayoutInflater inflater;
        //this is the id of the list view adapter's custom view (layout) (defined in: CountryLeaderBoardAdapterView.axml)
        private int resource;

        //constructor class for our adapter.  We pass in the context of the activity (controller) that is calling this fragment,
        //the id of the view (layout), and the world leader data set (DTOs)
        public WorldLeaderBoardAdapter(Context context, int resource, List<LeaderBoardUserContainer> leaders) : base(context, resource, leaders)
        {
            this.c = context;
            this.resource = resource;
            this.leaders = leaders;
        }
        //this method is responsible for setting the list view table's layout configuration details
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            //our inflator is responsible for creating our UI from WorldLeaderBoardAdapterView.axml 
            if (inflater == null)
            {
                inflater = (LayoutInflater)c.GetSystemService(Context.LayoutInflaterService);
            }
            //here we have our inflator create our view for us
            if (convertView == null)
            {
                convertView = inflater.Inflate(resource, parent, false);
            }
            //here we set the data that each row will hold
            WorldLeaderBoardRowContainer holder = new WorldLeaderBoardRowContainer(convertView)
            {
                username = { Text = leaders[position].user.userName },
                country = { Text = leaders[position].user.country },
                score = { Text = leaders[position].leaderboard.score.ToString() }
            };
            return convertView;
        }
       
    }
}
