﻿using System.Collections.Generic;
using System.Globalization;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using throwdown.Droid.Adapters;

/*
 This is a Fragment which is like a reusable controller/view that can be used within a normal controller/view.
 This Fragment's purposed is to show the Country leaderboard's "highest scores" by country for all users as 
 recorded by the backend server.
 */
namespace throwdown.Droid.Fragments
{
    public class CountryLeaderBoardFragment : Fragment
    {
        //the adapter binds our front end tabular data to a set of data transfer objects
        private CountryLeaderBoardAdapter _countryLeaderBoardAdapter;
        //this is our controller's tabular widget which will hold our table data
        private ListView _leaderBoardListView;
        //this is the set of data transfer objects that our adapter will bind to the front end tabular view
        private List<LeaderBoardUserContainer> _leaderBoardUserContainer;

        //this is called when our fragment is created
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        //this is called to allow us to wire up the view to the controller of this fragment
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            //create the country leaderboard fragments view.
            var view = inflater.Inflate(Resource.Layout.CountryLeaderBoardFragmentView, container, false);
            //set the tabular listview widget of this controller and wire it up to the front end listview widget
            _leaderBoardListView = (ListView)view.FindViewById(Resource.Id.worldLeaderBoardListView);

            //get country data from Android OS
            var globalization = RegionInfo.CurrentRegion;
            string country = globalization.NativeName;

            //Get country name text view widget for this controller and wire up to front end text view widget
            TextView countryLeaderBoardTextView = (TextView)view.FindViewById(Resource.Id.countryLeaderBoardTextView);
            countryLeaderBoardTextView.Text = country;

            //Set the country leaderboard's adapter to this fragment, set the layout for the table, and then load the data into it
            _countryLeaderBoardAdapter = new CountryLeaderBoardAdapter(this.Activity, Resource.Layout.CountryLeaderBoardAdapterView, GetCountryLeaders(country));
            _leaderBoardListView.Adapter = _countryLeaderBoardAdapter;

            return view;
        }
        //this method gets the country leader data from the server for this country for use in table
        private List<LeaderBoardUserContainer> GetCountryLeaders(string country)
        {
            _leaderBoardUserContainer = RestService.GetCountryLeaderBoard(country);
            if(_leaderBoardUserContainer == null || _leaderBoardUserContainer.Count == 0)
            {
                Toast.MakeText(this.Activity, "The registration server is currently down or you are not connected to the internet, please try again.", ToastLength.Long).Show();
            }
            return _leaderBoardUserContainer;
        }
    }
}
