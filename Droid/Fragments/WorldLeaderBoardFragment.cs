﻿using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using throwdown.Droid.Adapters;

/*
 This is a Fragment which is like a reusable controller/view that can be used within a normal controller/view.
 This Fragment's purposed is to show the World leaderboard's "highest scores" by country for all users as 
 recorded by the backend server.
 */
namespace throwdown.Droid.Fragments
{
    public class WorldLeaderBoardFragment : Fragment
    {
        //the adapter binds our front end tabular data to a set of data transfer objects
        private WorldLeaderBoardAdapter _worldLeaderBoardAdapter;
        //this is our controller's tabular widget which will hold our table data
        private ListView _leaderBoardListView;
        //this is the set of data transfer objects that our adapter will bind to the front end tabular view
        private List<LeaderBoardUserContainer> _leaderBoardUserContainer;

        //this is called when our fragment is created
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        //this is called to allow us to wire up the view to the controller of this fragment
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {           
            //create the world leaderboard fragments view.
            var view = inflater.Inflate(Resource.Layout.WorldLeaderBoardFragmentView, container, false);
            //set the tabular listview widget of this controller and wire it up to the front end listview widget
            _leaderBoardListView = (ListView)view.FindViewById(Resource.Id.worldLeaderBoardListView);
                 
            //Set the world leaderboard's adapter to this fragment, set the layout for the table, and then load the data into it
            _worldLeaderBoardAdapter = new WorldLeaderBoardAdapter(this.Activity, Resource.Layout.WorldLeaderBoardAdapterView, GetWorldLeaders());
            _leaderBoardListView.Adapter = _worldLeaderBoardAdapter;

            return view;
        }
        //this method gets the world leader data from the server for this country for use in table
        private List<LeaderBoardUserContainer> GetWorldLeaders()
        {
            _leaderBoardUserContainer = RestService.GetWorldLeaderBoard();
            if (_leaderBoardUserContainer == null || _leaderBoardUserContainer.Count == 0)
            {
                Toast.MakeText(this.Activity, "The registration server is currently down or you are not connected to the internet, please try again.", ToastLength.Long).Show();
            }
            return _leaderBoardUserContainer;
        }
    }
}
