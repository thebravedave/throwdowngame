﻿using System.Collections.Generic;
using System.Globalization;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using throwdown.Droid.Adapters;

/*
 This is a Fragment which is like a reusable controller/view that can be used within a normal controller/view.
 This Fragment's purposed is to show the user's "highest scores" for all the user's throws
 recorded by the backend server.
 */
namespace throwdown.Droid.Fragments
{
    public class MyScoresFragment : Fragment
    {
        //the adapter binds our front end tabular data to a set of data transfer objects
        private MyScoresAdapter _myScoresAdapter;
        //this is our controller's tabular widget which will hold our table data
        private ListView _myScoresListView;
        //this is the set of data transfer objects that our adapter will bind to the front end tabular view
        private List<UserThrow> _leaderBoardUserContainer;

        //this is called when our fragment is created
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        //this is called to allow us to wire up the view to the controller of this fragment
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            //create the user throws leaderboard fragments view.
            var view = inflater.Inflate(Resource.Layout.MyScoresFragmentView, container, false);
            //set the tabular listview widget of this controller and wire it up to the front end listview widget
            _myScoresListView = (ListView)view.FindViewById(Resource.Id.MyScoresListView);

            //get country data from Android OS
            var globalization = RegionInfo.CurrentRegion;
            string country = globalization.NativeName;

            //Get country name text view widget for this controller and wire up to front end text view widget
            TextView countryMyScoresTextView = (TextView)view.FindViewById(Resource.Id.countryMyScoresTextView);
            countryMyScoresTextView.Text = country;

            //gets the user id from the app persisted settings to be sent to server to identify which user to get throws for
            ISharedPreferences prefs = Application.Context.GetSharedPreferences("APPLICATION_PREFERENCES", FileCreationMode.Private);
            int userId = prefs.GetInt("user_id", 0);

            //Set the "my throws" leaderboard's adapter to this fragment, set the layout for the table, and then load the data into it
            _myScoresAdapter = new MyScoresAdapter(this.Activity, Resource.Layout.MyScoresAdapterView, GetUserThrows(userId.ToString()));
            _myScoresListView.Adapter = _myScoresAdapter;

            return view;
        }
        //this method gets the "my throws" data from the server for this userfor use in table
        private List<UserThrow> GetUserThrows(string userId)
        {
            _leaderBoardUserContainer = RestService.GetUserThrows(userId);
            if (_leaderBoardUserContainer == null)
            {
                
                Toast.MakeText(this.Activity, "The registration server is currently down or you are not connected to the internet, please try again.", ToastLength.Long).Show();
            }
            if(_leaderBoardUserContainer.Count == 0)
            {
                Toast.MakeText(this.Activity, "You haven't made any throws yet.", ToastLength.Long).Show();
            }
            return _leaderBoardUserContainer;
        }
    }
}
