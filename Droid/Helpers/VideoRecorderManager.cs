﻿using System;
using System.IO;
using Android.App;
using Android.Content;
using Android.Media;
using Android.Views;
using Android.Widget;

/*
 This class is responsible for recording video for each throw as long as the user has
 specified that they want video recording in the settings page
 */
namespace throwdown.Droid.Helpers
{
    public class VideoRecorderManager
    {
        //MediaRecord is the Android class that handles the actual recording of the video for us
        MediaRecorder _recorder;
        //path is where we want the video to be saved
        string path;
        //ISharedPreferences is the class that is responsible for getting/setting persisted app settings
        ISharedPreferences prefs;
        //this is a boolean that will determine if we should save video.  This will be based off of their video recording peferences from the settings controller/view
        bool _saveVideoSettings = false;

        //This is our constructor for our VideoRecordingManager.  
        public VideoRecorderManager()
        {
            //here we instantiate our MediaRecorder
            _recorder = new MediaRecorder();
            //here we get our persisted app settings.
            prefs = Application.Context.GetSharedPreferences("APPLICATION_PREFERENCES", FileCreationMode.Private);
            //from persisted app settings determine if they want video to be recorded for throws
            _saveVideoSettings = prefs.GetBoolean("save_video_settings", false);
        }
        //this is the method that is responsible for setting up then starting the recording of the video
        public bool RecordVideo(VideoView video)
        {
            //we check if they allow saving of videos from their settings page (set in constructor)
            if (_saveVideoSettings)
            {
                try
                {
                    //here we get the path to their internal DCIM folder
                    string dcimPath = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDcim).AbsolutePath;
                    //here we set the final path where the video will be saved, which is in the internal storate: /DCIM/Throwdown
                    string finalPathWithFileName = dcimPath + "/" + "Throwdown";
                    //Here we create the Throwdown directory if it doesn't exist
                    Directory.CreateDirectory(finalPathWithFileName);
                    //here we set the final path with the temp video file name
                    finalPathWithFileName = finalPathWithFileName + "/tmp.mp4";

                    //Here we set the MediaRecorder and it's settings
                    _recorder = new MediaRecorder();
                    _recorder.SetVideoSource(VideoSource.Camera);
                    _recorder.SetAudioSource(AudioSource.Mic);
                    _recorder.SetOutputFormat(OutputFormat.Default);
                    _recorder.SetVideoEncoder(VideoEncoder.Default);
                    _recorder.SetAudioEncoder(AudioEncoder.Default);
                    _recorder.SetOutputFile(finalPathWithFileName);
                    Surface surface = video.Holder.Surface;
                    _recorder.SetPreviewDisplay(surface);
                    _recorder.Prepare();
                    //start the recording process
                    _recorder.Start();

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
            }
            return true;
        }
        //This is the method that is called after they finish their throw and we want to stop recording the throw video
        public bool StopRecordingVideo(float score, float duration)
        {
            try
            {
                if (_recorder != null)
                {
                    //stop the recording and release the recorder
                    _recorder.Stop();
                    _recorder.Release();
                    _recorder.Dispose();
                    _recorder = null;

                    string dcimPath = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDcim).AbsolutePath;

                    //we are going to give the video a very distinct name of: YYYY_MM_DD_score_[actualscore]_duration_[actualduration].mp4
                    DateTime dateTime = DateTime.Now;
                    string date = dateTime.ToString();
                    date = date.Replace("/", "_");
                    string previousVideoName = "Throwdown/tmp.mp4";
                    string previousPath = Path.Combine(dcimPath, previousVideoName);

                    string fileName = "Throwdown/" + date + "_score_" + score.ToString() + "_duration_" + duration.ToString() + ".mp4";
                   
                    path = Path.Combine(dcimPath, fileName);
                    //replace all spaces in the date with underscores
                    path = path.Replace(" ", "_");

                    //replace the temp video file tmp.mp4 with our custom name
                    System.IO.File.Move(previousPath, path);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return true;
        }
    }
}