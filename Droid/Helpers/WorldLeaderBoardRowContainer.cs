﻿using System;
using Android.Views;
using Android.Widget;

/*
 This class is the class that will be created by our adapter and will be the layout for each row of our World leaderboard
*/
namespace throwdown.Droid.Helpers
{
    public class WorldLeaderBoardRowContainer
    {
        public TextView username;
        public TextView score;
        public TextView country;

        public WorldLeaderBoardRowContainer(View v)
        {
            //these next three lines are our score, country, and username fields for each row for the world leaderboard table
            username = v.FindViewById<TextView>(Resource.Id.worldLeaderBoardUserName);
            country = v.FindViewById<TextView>(Resource.Id.worldLeaderBoardCountry);
            score = v.FindViewById<TextView>(Resource.Id.worldLeaderBoardScore);
         
        }
    }
}
