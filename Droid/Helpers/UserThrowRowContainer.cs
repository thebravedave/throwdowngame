﻿using System;
using Android.Views;
using Android.Widget;

/*
 This class is the class that will be created by our adapter and will be the layout for each row of our User Throws leaderboard
*/
namespace throwdown.Droid.Helpers
{
    public class UserThrowRowContainer
    {
        public TextView throwDate;
        public TextView score;
        public TextView duration;

        public UserThrowRowContainer(View v)
        {
            //these next three lines are our score, throwdate, and duration fields for each row for the user throws leaderboard table
            score = v.FindViewById<TextView>(Resource.Id.myScoresScore);
            throwDate = v.FindViewById<TextView>(Resource.Id.myScoresDate);
            duration = v.FindViewById<TextView>(Resource.Id.myScoresDuration);
        }
    }
}
