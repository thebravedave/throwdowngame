﻿using System;
using Android.Views;
using Android.Widget;

/*
 This class is the class that will be created by our adapter and will be the layout for each row of our country leaderboard
*/
namespace throwdown.Droid.Helpers
{
    public class CountryLeaderBoardRowContainer
    {
        public TextView username;
        public TextView score;

        public CountryLeaderBoardRowContainer(View v)
        {
            //these next two lines are our username and score fields for each row for the country leaderboard table
            username = v.FindViewById<TextView>(Resource.Id.countryLeaderBoardUserName);
            score = v.FindViewById<TextView>(Resource.Id.countryLeaderBoardScore);
        }
    }
}
