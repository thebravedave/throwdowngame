using Foundation;
using System;
using UIKit;
/*
 This controller is responsible for profile information page in the settings
 page. It handles the capabilities associated if the user wishes to change their
 username
 */
namespace throwdown.iOS
{
    public partial class ProfileInfoController : UIViewController
    {
        NSUserDefaults storeResults;
        int userId;
        string userCountry;
        string name;

        public ProfileInfoController(IntPtr handle) : base(handle)
        {

            storeResults = NSUserDefaults.StandardUserDefaults;
            //get the user id, username and user's country to be used if user requests to change profile info
            userId = (int)storeResults.IntForKey("userID");
            userCountry = storeResults.StringForKey("userCountry");
            name = storeResults.StringForKey("userName");

        }
        // method that gets called when view controller gets loaded.
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // populate fields with appropriate data
            countryProfile.Text = userCountry;
            usernameProfile.Text = name;

            countryProfile.UserInteractionEnabled = false;
            usernameProfile.UserInteractionEnabled = false;
            // only edit button is visible orignally
            saveButton.Hidden = true;

            // if edit button is clicked then it becomes hidden, and save becomes visible
            // and user interaction is enabled for username (so that user can edit it)
            editButton.TouchUpInside += (object sender, EventArgs e) =>
            {               
                usernameProfile.UserInteractionEnabled = true;
                // keyboard cursor goes straight to this username field
                usernameProfile.BecomeFirstResponder();
                editButton.Hidden = true;
                saveButton.Hidden = false;
                saveButton.UserInteractionEnabled = true;
                usernameProfile.ReturnKeyType = UIReturnKeyType.Done;

            };

            // if user saves changes that were made to the username, checks must be made that it
            // was a valid username. Alert user whether changes were invalid or successful
            saveButton.TouchUpInside += (object sender, EventArgs e) =>
             {
                // call to shared code to update the user's name
                Updated update = RestService.UpdateUseridCountry(userId.ToString(), userCountry, usernameProfile.Text);

                // either server down or no internet service
                if (update == null)
                 {
                     var okAlertController = UIAlertController.Create("Error", "There are connectivity issues, please try again", UIAlertControllerStyle.Alert);
                     //Add Action
                     okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                     // Present Alert
                     PresentViewController(okAlertController, true, null);
                     return;
                 }

                // username is taken
                if (update.numberUpdated == 0)
                 {
                     var okAlertController = UIAlertController.Create("Invalid Username", "Sorry that username is taken, please try again", UIAlertControllerStyle.Alert);
                     //Add Action
                     okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                     // Present Alert
                     PresentViewController(okAlertController, true, null);
                     return;
                 }

                 // otherwise it was success
                 else
                 {
                     var okAlertController = UIAlertController.Create("Success", "The username was successfully saved", UIAlertControllerStyle.Alert);
                     //Add Action
                     okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                     // Present Alert
                     PresentViewController(okAlertController, true, null);

                     //orignal state of application
                     editButton.Hidden = false;
                     saveButton.Hidden = true;
                     usernameProfile.UserInteractionEnabled = false;
                     //save new username locally
                     storeResults.SetString(usernameProfile.Text, "userName");
                     return;
                 }
            };


            usernameProfile.ShouldReturn = delegate
            {
                // move the text entry to the next field.
                usernameProfile.ResignFirstResponder();
                return true;
            };
        }
    }
}

