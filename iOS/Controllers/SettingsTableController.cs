using Foundation;
using System;
using UIKit;

namespace throwdown.iOS
{
    public partial class SettingsTableController : UITableViewController
    {
        UITableView table;
        string[] tableItems = new string[] { "General", "Leaderboard", "Profile" };

        public SettingsTableController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            table = new UITableView(View.Bounds);//, UITableViewStyle.Grouped); // defaults to Plain style
            table.Source = new TableSource(tableItems, this);
            Add(table);
        }





    }
}