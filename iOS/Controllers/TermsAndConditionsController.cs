using System;
using UIKit;

/*
 Controller associated with the terms and conditions page
 */
namespace throwdown.iOS
{
    public partial class TermsAndConditionsController : UIViewController
    {
        public TermsAndConditionsController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            // total number of lines for terms and conditions
            terms.Lines = 28;

        }
    }
}