using System;
using UIKit;
using System.Collections.Generic;
using Foundation;

/*
CountryLeaderController is responsible for populating the user's country leaderboard table.
This is done by making a call to GetCountryLeaderBoard() from the shared code and parsing
the return value (List<LeaderBoardUserContainer>).
*/
namespace throwdown.iOS
{
    public partial class CountryLeaderController : UIViewController
    {
        NSUserDefaults storeResults;
        // tableItems is what is passed into TwoTableSourceLeaderboard() to create the table
        List<string[]> tableItems = new List<string[]>();
        String theCountry;

        public CountryLeaderController(IntPtr handle) : base(handle)
        {
            // user's country is saved locally on the phone
            storeResults = NSUserDefaults.StandardUserDefaults;
            theCountry = storeResults.StringForKey("userCountry");

            string[] firstItem = new string[] { "0", "0" };
            // titles for the table
            firstItem[0] = "Username";
            firstItem[1] = "Score";
            tableItems.Add(firstItem);

            List<LeaderBoardUserContainer> lb = RestService.GetCountryLeaderBoard(theCountry);
            // lb.Count will be 50 if entries exist
            for (int i = 0; i < lb.Count; i++)
            {
                //each entry will consist of a username and a score
                string[] rowItem = new string[] { "0", "0" };
                rowItem[0] = lb[i].user.userName;
                rowItem[1] = lb[i].leaderboard.score.ToString();
                tableItems.Add(rowItem);
            }


        }
        // method that gets called when view controller gets loaded.
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // set the countryTable (UITableView)
            countryTable.Source = new TwoTableSourceLeaderboard(tableItems);
        }
    }
}