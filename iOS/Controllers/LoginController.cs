using System;
using UIKit;
using Foundation;

/*
 Controller associated with the login screen. Successful completion through this screen means that the user entered
 a valid username which will be saved locally on the phone (login process is only completed once). Country
 user currently is in is also saved locally to the phone to be used for leaderboards.
 */
namespace throwdown.iOS
{

    public partial class LoginController : UIViewController
    {
        NSUserDefaults storeResults;
        public string countryDesc;

        public LoginController (IntPtr handle) : base (handle)
        {
            
        }

        // method that gets called when view controller gets loaded. Wires up button logic.
        public override void ViewDidLoad()
        {
            // local file on phone
            storeResults = NSUserDefaults.StandardUserDefaults;
            // store country user is currently in
            countryDesc = NSLocale.CurrentLocale.GetCountryCodeDisplayName(NSLocale.CurrentLocale.CountryCode);
            //formatting for title font
            welcomeTitle.Font = UIFont.FromName("Bangers-Regular", 35f);
            // keyboard will have a done option
            theUsername.ReturnKeyType = UIReturnKeyType.Done;
            // handler if submit button is clicked
            SubmitButton.TouchUpInside += (object sender, EventArgs e) => {

                Added id = null;

                // didn't accept terms and conditions alert
                if (!acceptTerms.On)
                {
                    var okAlertController = UIAlertController.Create("Invalid Registration", "Please agree to the terms and conditions", UIAlertControllerStyle.Alert);
                    //Add Action
                    okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                    // Present Alert
                    PresentViewController(okAlertController, true, null);
                    return;
                }

                // access webservice to try to register the username
                id = RestService.RegistrationUsernameCountry(theUsername.Text.Trim(), countryDesc);

                // condition either no wifi or server is down
                if (id == null)
                {
                    var okAlertController = UIAlertController.Create("Connectivity Issue", "Sorry, please try again later", UIAlertControllerStyle.Alert);
                    //Add Action
                    okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                    // Present Alert
                    this.NavigationController.PresentViewController(okAlertController, true, null);
                    return;
                }

                // username already is taken
                if(id.id == 0)
                {
                    var okAlertController = UIAlertController.Create("Invalid Username", "That username is already taken or is invalid, please try again", UIAlertControllerStyle.Alert);
                    //Add Action
                    okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                    // Present Alert
                    this.NavigationController.PresentViewController(okAlertController, true, null);
                    return;
                   
                }

                // store results for later use of the app
                storeResults.SetInt(id.id, "userID");
                storeResults.SetString(theUsername.Text, "userName");
                storeResults.SetString(countryDesc, "userCountry");

                //proceed to instructions if success with username
                var storyBoard = UIStoryboard.FromName("LoginStory", null);
                Instructions1Controller instructionsController = (Instructions1Controller)storyBoard.InstantiateViewController("help1");
                NavigationController.PushViewController(instructionsController, true);

            };



            theUsername.ShouldReturn = delegate
            {
                // move the text entry to the next field.
                theUsername.ResignFirstResponder();
                return true;
            };

        }
    }
}