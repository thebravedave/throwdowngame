using Foundation;
using System;
using UIKit;
/*
 Skeleton controller for the main menu. Included for future versions if main menu needs to contain 
 extra features.
 */
namespace throwdown.iOS
{
    public partial class MainMenuController : UIViewController
    {
        public MainMenuController (IntPtr handle) : base (handle)
        {
        }
    }
}