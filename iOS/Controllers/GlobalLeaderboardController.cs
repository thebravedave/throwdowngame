using System;
using UIKit;
using System.Collections.Generic;

/*
GlobalLeaderController is responsible for populating the global leaderboard table.
This is done by making a call to GetWorldLeaderBoard() from the shared code and parsing
the return value (List<LeaderBoardUserContainer>).
*/
namespace throwdown.iOS
{
    public partial class GlobalLeaderboardController : UIViewController
    {
        // tableItems is what is passed into TableSourceLeaderboard() to create the table
        List<string[]> tableItems = new List<string[]>();


        public GlobalLeaderboardController (IntPtr handle) : base (handle)
        {
            // firstItem is table title information
            string[] firstItem = new string[] { "0", "0", "0" };
            firstItem[0] = "Username";
            firstItem[1] = "Country";
            firstItem[2] = "Score";
            tableItems.Add(firstItem);
                      
            List<LeaderBoardUserContainer> lb = RestService.GetWorldLeaderBoard();
            // lb.Count will be 50 if entries exist
            for (int i =0; i < lb.Count; i++ ){
                // each entry will have username, country and a score
                string[] rowItem = new string[]{"0", "0", "0"};
                rowItem[0] = lb[i].user.userName;
                rowItem[1] = lb[i].user.country;
                rowItem[2] = lb[i].leaderboard.score.ToString();
                tableItems.Add(rowItem);
            }

           
        }
        // method that gets called when view controller gets loaded.
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // set the globalTable (UITableView)
            globalTable.Source = new TableSourceLeaderboard(tableItems);

        }
    }
}



