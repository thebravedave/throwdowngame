using Foundation;
using System;
using UIKit;

namespace throwdown.iOS
{
    public partial class NavController : UINavigationController
    {
        public NavController (IntPtr handle) : base (handle)
        {
        }
        public override void ViewDidLoad()
        {
            this.SetNavigationBarHidden(true, false);
        }
    }
}
