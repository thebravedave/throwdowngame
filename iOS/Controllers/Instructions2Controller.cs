using System;
using UIKit;

/*
 Controller for the second instructions page after login. Contains simple formatting.
 */
namespace throwdown.iOS
{
    public partial class Instructions2Controller : UIViewController
    {
        public Instructions2Controller(IntPtr handle) : base(handle)
        {
        }
        // method that gets called when view controller gets loaded.
        public override void ViewDidLoad()
        {
            //formatting for instruction description
            description.Lines = 2; 
            //formatting for title font
            tutorialTitle.Font = UIFont.FromName("Bangers-Regular", 35f);
        }
    }
}
