using Foundation;
using System;
using UIKit;
using CoreMotion;
using System.Collections.Generic;
using System.Diagnostics;

/*
 This controller is responsible for the logic behind using the accelerometer and data 
storage during game play.
*/
namespace throwdown.iOS
{
    public partial class GameController : UIViewController
    {
        // the max x, y and z has seen for a single throw
        double maxX = 0;
        double maxY = 0;
        double maxZ = 0;
        // boolean value that changes when user starts game by swiping up
        bool swipedUP = false;
        // highest score of x y or z
        double overallMax = 0;
        // boolean to decide if accelerometer data should be collected
        bool startCollecting = false;
        // boolean whether the user has started the throw
        bool throwStarted = false;

        Stopwatch a;
        // storage of acceleration data
        List<double[]> accelerationData;
        CMMotionManager motionManager;

        NSUserDefaults storeResults;
        int userId;
        string userCountry;

        public GameController(IntPtr handle) : base(handle)
        {
            storeResults = NSUserDefaults.StandardUserDefaults;
            // measuring the time of a throw
            a = new Stopwatch();
            // class used to get accelerometer data
            motionManager = new CMMotionManager();
            // acceleration data storage
            accelerationData = new List<double[]>();
            // user id and country saved locally on the phone
            userId = (int)storeResults.IntForKey("userID");
            userCountry = storeResults.StringForKey("userCountry");
            // Title name
            NavigationItem.Title = "Game Screen";

            // set up the top bar's appearance and navigation capabilities to main menu
            if (NavigationController != null)
            {
                NavigationController.NavigationBar.BarStyle = UIBarStyle.Black;
                NavigationController.NavigationBar.Translucent = true;
                NavigationItem.SetRightBarButtonItem(new UIBarButtonItem(UIBarButtonSystemItem.Action, (sender, args) =>
                {
                    // if user navigates away from game screen stop collecting data and switch to main menu view
                    startCollecting = false;
                    var storyBoard = UIStoryboard.FromName("MainMenuStoryboard", null);
                    MainMenuController loginController = (MainMenuController)storyBoard.InstantiateViewController("mainMenu");
                    NavigationController.PushViewController(loginController, true);
                }), true);
            }

        }

        // method that gets called when view controller gets loaded - start accelerometer updates and data analysis
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // recognize swipe up
            WireUpDragGestureRecognizer();
            didNotSwipe.Font = UIFont.FromName("Bangers-Regular", 20f);
            throwTitle.Font = UIFont.FromName("Bangers-Regular", 35f);

            //note that the following three labels are set as hidden for this release
            // but represent the current x y and z acceleration
            this.xMax.Text = "0";
            this.yMax.Text = "0";
            this.zMax.Text = "0";
            // label on screen that shows the max acceleration in either the x, y or z direction
            this.totalMax.Text = "0";

            //start collecting accelerometer data if phone's hardware is capable       
            if (motionManager.AccelerometerAvailable)
            {
                // gets accelerometer updates every .1 second
                motionManager.AccelerometerUpdateInterval = .1;

                motionManager.StartAccelerometerUpdates(NSOperationQueue.CurrentQueue, (data, error) =>
                {
                    startCollecting = true;
                    collectData(data);

                });
            }
        }

        // called within collectData() used to determine when a throw starts/finishes
        public void analyzeData()
        {
            // worthy to start measuring if acceleration breaks a certain threshold
            bool worthyOfStarting = (Math.Abs(accelerationData[accelerationData.Count - 1][0]) > 1 *7|| Math.Abs(accelerationData[accelerationData.Count - 1][1]) > 1 *7|| Math.Abs(accelerationData[accelerationData.Count - 1][2]) > 1 *7);
            // if breaks threshold and throw hasn't started - start measuring throw!
            if(worthyOfStarting & !throwStarted){
                throwStarted = true;
                a.Start();
            }
            // if throw has started and acceleration does not break threshold - end throw!
            if(throwStarted & !worthyOfStarting ){
                a.Stop();
                int time = a.Elapsed.Milliseconds;
                // only notify user if throw breaks a certain threshold
                if (overallMax > 2 *7)
                {
                    string message;
                    string title;
                    // send throw details to web service
                    UserThrowAddDetails dets = RestService.AddScore(userId.ToString(), overallMax.ToString(), time.ToString());
                    // if web service returns null then alert user there are connectivity issues
                    if (dets == null)
                    {
                        message = "There are issues connecting to the server please try again later";
                        title = "Connectivity Issue";

                    }
                    // else throw was good and alert user on their performance
                    else
                    {
                        title = "Nice Throw";
                        string madeLeaderboard = "";
                        if(dets.leaderBoardAdded.id != 0){
                             madeLeaderboard = "Congrats you made the leaderboard!";
                        }
                        message = "Time: " + time.ToString() + " ms\n Score: " + Math.Round(overallMax, 2).ToString() + "\nPercentile: " + Math.Round(dets.percentile, 2).ToString() + "%\n" + madeLeaderboard;
                    }
                    // display alert
                    var okAlertController = UIAlertController.Create(title , message, UIAlertControllerStyle.Alert);
                    okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                    PresentViewController(okAlertController, true, null);

                    //reset game to orignal values
                    overallMax = 0;
                    setToZero();
                    throwStarted = false;
                    swipedUP = false;
                }
            }   
        }

        //reset to start conditions
        public void setToZero()
        {
            maxX = 0;
            maxY = 0;
            maxZ = 0;
            this.totalMax.Text = "0";
            this.xMax.Text = maxX.ToString();
            this.yMax.Text = maxY.ToString();
            this.zMax.Text = maxZ.ToString();
        }

        //if user navigates away from game reset to start conditions
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
            setToZero();
            swipedUP = false;
            startCollecting = false;
        }

        // following two functions used to recognize a swipe up
        private void WireUpDragGestureRecognizer()
        {
            // Wire up the event handler (have to use a selector)
            swipeUp.AddTarget(() => HandleDrag(swipeUp));
        }
        private void HandleDrag(UIPanGestureRecognizer recognizer)
        {
            swipedUP = true;
            //Console.WriteLine("I swiped up");
        }

        // called every time acceleration data is read (.1 seconds)
        public void collectData(CMAccelerometerData data)
        {
            // do not let accelerometer readings storage get too large
            if(accelerationData.Count > 1000){
                accelerationData = new List<double[]>();
            }
            // if user swiped up game started
            if (swipedUP)
            {
                // remove label prompting user to swipe up
                didNotSwipe.Hidden = true;

                // only start collecting if specified
                if (startCollecting)
                {
                    //put into datastructure because start button has been clicked
                    double[] dataPoint = new double[3];
                    // absolute value of acceleration with 7 being constant to normalize for android scores
                    double xVal = Math.Abs(data.Acceleration.X) *7;
                    double yVal = Math.Abs(data.Acceleration.Y) *7;
                    double zVal = Math.Abs(data.Acceleration.Z) *7;

                    dataPoint[0] = xVal;
                    dataPoint[1] = yVal;
                    dataPoint[2] = zVal;

                    //update the UI label (hidden in this version)
                    this.yMax.Text = Math.Round(yVal, 2).ToString();
                    this.xMax.Text = Math.Round(xVal, 2).ToString();
                    this.zMax.Text = Math.Round(zVal, 2).ToString();

                    //update x values accordingly
                    if (xVal > maxX)
                    {
                        maxX = xVal;
                        // update overallMax if necessary
                        if (maxX > overallMax)
                        {
                            overallMax = maxX;
                        }
                    }
                    //update y values accordingly
                    if (yVal > maxY)
                    {
                        maxY = yVal;
                        // update overallMax if necessary
                        if (maxY > overallMax)
                        {
                            overallMax = maxY;
                        }
                    }
                    //update z values accordingly
                    if (zVal > maxZ)
                    {
                        maxZ = zVal;
                        // update overallMax if necessary
                        if (maxZ > overallMax)
                        {
                            overallMax = maxZ;
                        }
                    }

                    // update totalMax label
                    this.totalMax.Text = Math.Round(overallMax,2).ToString();
                    accelerationData.Add(dataPoint);
                    //analyze accel data to determine when throw finished/started
                    analyzeData();

                }
            }
            // no need for data analysis if user did not swipe up to start the game
            else{
                didNotSwipe.Hidden = false;
            }
        }
    }
}