// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace throwdown.iOS
{
    [Register ("GameController")]
    partial class GameController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel didNotSwipe { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView startGame { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPanGestureRecognizer swipeUp { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel throwTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel totalMax { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel xMax { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel yMax { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel zMax { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (didNotSwipe != null) {
                didNotSwipe.Dispose ();
                didNotSwipe = null;
            }

            if (startGame != null) {
                startGame.Dispose ();
                startGame = null;
            }

            if (swipeUp != null) {
                swipeUp.Dispose ();
                swipeUp = null;
            }

            if (throwTitle != null) {
                throwTitle.Dispose ();
                throwTitle = null;
            }

            if (totalMax != null) {
                totalMax.Dispose ();
                totalMax = null;
            }

            if (xMax != null) {
                xMax.Dispose ();
                xMax = null;
            }

            if (yMax != null) {
                yMax.Dispose ();
                yMax = null;
            }

            if (zMax != null) {
                zMax.Dispose ();
                zMax = null;
            }
        }
    }
}