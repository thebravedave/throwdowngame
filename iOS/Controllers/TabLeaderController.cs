using Foundation;
using System;
using UIKit;

/*
 Controller used to manage the tabs for leaderboard. This controller contains
 code for formatting.
 */
namespace throwdown.iOS
{
    public partial class TabLeaderController : UITabBarController
    {
        public TabLeaderController (IntPtr handle) : base (handle)
        {

        }

        public override void ViewDidLoad()
        {   // titles of the each tab
            theTabBar.Items[0].Title = "Global";
            theTabBar.Items[1].Title = "Country";
            theTabBar.Items[2].Title = "My History";

            theTabBar.BarStyle = UIBarStyle.Black;

        }

    }
}