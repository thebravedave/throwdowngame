// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace throwdown.iOS
{
    [Register ("ProfileInfoController")]
    partial class ProfileInfoController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField countryProfile { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton editButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton saveButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField usernameProfile { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (countryProfile != null) {
                countryProfile.Dispose ();
                countryProfile = null;
            }

            if (editButton != null) {
                editButton.Dispose ();
                editButton = null;
            }

            if (saveButton != null) {
                saveButton.Dispose ();
                saveButton = null;
            }

            if (usernameProfile != null) {
                usernameProfile.Dispose ();
                usernameProfile = null;
            }
        }
    }
}