using Foundation;
using System;
using UIKit;
using System.Collections.Generic;

/*
PersonalLeaderboardController is responsible for populating the user's personal leaderboard table.
This is done by making a call to GetUserThrows() from the shared code and parsing
the return value.
*/
namespace throwdown.iOS
{
    public partial class PersonalLeaderboardController : UIViewController
    {
        NSUserDefaults storeResults;
        List<string[]> tableItems = new List<string[]>();
        int uID;


        public PersonalLeaderboardController(IntPtr handle) : base(handle)
        {

            storeResults = NSUserDefaults.StandardUserDefaults;
            // get user id which is saved locally
            uID = (int)storeResults.IntForKey("userID"); 
            // table title information
            string[] firstItem = new string[] { "0", "0", "0" };
            firstItem[0] = "Date";
            firstItem[1] = "Duration (ms)";
            firstItem[2] = "Score";
            tableItems.Add(firstItem);

            List<UserThrow> lb = RestService.GetUserThrows(uID.ToString());
            // lb.Count will be 10, if entries exist
            for (int i = 0; i < lb.Count; i++)
            {
                // each entry will have throw date, duration and score.
                string[] rowItem = new string[] { "0", "0", "0"};
                rowItem[0] = lb[i].throwDate.ToShortDateString();
                rowItem[1] = lb[i].throwDuration.ToString();
                rowItem[2] = lb[i].score.ToString();
                tableItems.Add(rowItem);
            }


        }
        // method that gets called when view controller gets loaded - load table.
        public override void ViewDidLoad()
        {

            base.ViewDidLoad();
            // set the personalTable (UITableView)
            personalTable.Source = new TableSourceLeaderboard(tableItems);

        }
    }
}