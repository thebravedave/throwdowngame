using Foundation;
using System;
using UIKit;
/*
 This controller is responsible for checking if the user has succesfully logged in before.
 If there was a successful login the User's user id would have been saved locally on to 
 the phone
*/
namespace throwdown.iOS
{
    public partial class SwitchController : UIViewController
    {
        public SwitchController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            
            NSUserDefaults storeResults = NSUserDefaults.StandardUserDefaults;
            int username = (int)storeResults.IntForKey("userID");

            // no username exisits
            if (username == 0)
            {
                //show login chain
                var storyBoard = UIStoryboard.FromName("LoginStory", null);
                LoginController loginController = (LoginController)storyBoard.InstantiateViewController("theLogin");
                NavigationController.PushViewController(loginController, true);
            }
            // user id exists so go straight to game
            else{
                PerformSegue("gameSegue", this);
            }

        }
    }
}