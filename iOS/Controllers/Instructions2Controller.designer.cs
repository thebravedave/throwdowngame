// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace throwdown.iOS
{
    [Register ("Instructions2Controller")]
    partial class Instructions2Controller
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel description { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton PlayGame { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel tutorialTitle { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (description != null) {
                description.Dispose ();
                description = null;
            }

            if (PlayGame != null) {
                PlayGame.Dispose ();
                PlayGame = null;
            }

            if (tutorialTitle != null) {
                tutorialTitle.Dispose ();
                tutorialTitle = null;
            }
        }
    }
}