// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace throwdown.iOS
{
    [Register ("CountryLeaderController")]
    partial class CountryLeaderController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView countryTable { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (countryTable != null) {
                countryTable.Dispose ();
                countryTable = null;
            }
        }
    }
}