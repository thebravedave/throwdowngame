﻿This folder contains the iOS part of "Throwdown", a phone game where the goal is to throw your phone as hard as 
possible to maximize your score. The primary subfolders that contain the core code are "Controllers", "CreatingTable" 
and "Storyboards". All the views in "Stroyboards" have classes associated to them in "Controllers". "CreatingTable" is
responsible for formatting the leaderboards. The iOS portion makes use of "shared code" which holds the logic that
makes calls to the RESTful webservice which is run on an apache server.


Relationship between Storyboard and Controllers
------------------------------------------------

Storyboard              Description                                                                     Controllers

LaunchScreen            First storyboard loaded upon application launch.                                None
mainTD                  Initializes the application and redirects user based on previous login.         SwitchController, NavController
LoginStoryboard         Encompasses controllers associated with user registration process.              LoginController, Instructions1Controller, Instructions2Controller, TermsAndConditionsController
MainMenuStoryboard      Encompasses controllers associated with navigation of the application.          MainMenuController, TabLeaderController, CountryLeaderController, GlobalLeaderboardController, PersonalLeaderboardController, ProfileInfoController, MultiColumnCells, TwoColumnCells
theGame                 Encompasses controllers associated with accelerometer data, and scoring.        GameController GameNavController
