// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace throwdown.iOS
{
    [Register ("CreateLoginController")]
    partial class CreateLoginController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView LOGIN { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (LOGIN != null) {
                LOGIN.Dispose ();
                LOGIN = null;
            }
        }
    }
}