﻿using System.Drawing;
using UIKit;

/*
 This class is used to create 2 columns for a row for the country leaderboard table.
 */
namespace throwdown.iOS
{

    public class TwoColumnCell : UITableViewCell
    {
        public static readonly string ID = "TwoColumnCell";

        public UILabel LeftValue { get; set; }
        public UILabel RightValue { get; set; }

        public TwoColumnCell()
        {   
            // right and left cell based on height width and postion
            LeftValue = new UILabel(new RectangleF(0, 0, 250, 50));
            RightValue = new UILabel(new RectangleF(250, 0, 250, 50));

            //set the cells to have the same color as the background
            LeftValue.BackgroundColor = UIColor.FromRGB(88, 142, 200);
            RightValue.BackgroundColor = UIColor.FromRGB(88, 142, 200);

            //add cells to the view
            AddSubview(LeftValue);
            AddSubview(RightValue);
        }

    }
}



