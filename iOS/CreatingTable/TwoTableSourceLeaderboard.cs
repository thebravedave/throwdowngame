﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

/*
 This class is used to set the UITableView to the appropriate data. This class is
 used for the country leadboard as it has only two columns of data.
 */
namespace throwdown.iOS
{
    public class TwoTableSourceLeaderboard : UITableViewSource
    {

        List<string[]> TableItems;

        public TwoTableSourceLeaderboard(List<string[]> items)
        {
            TableItems = items;

        }

        //method from UITableViewSource that must be overriden
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return TableItems.Count;
        }

        //method from UITableViewSource that must be overriden. Called when each cell is being set
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            // checking to see if there is a cell that can be reused.
            var cell = tableView.DequeueReusableCell(TwoColumnCell.ID) as TwoColumnCell;
            // if cell has not been used create new cell
            if (cell == null)
            {
                cell = new TwoColumnCell();
            }
            // set the values in the cell
            cell.LeftValue.Text = TableItems[indexPath.Row][0];
            cell.RightValue.Text = TableItems[indexPath.Row][1];
            return cell;
        }



    }
}
