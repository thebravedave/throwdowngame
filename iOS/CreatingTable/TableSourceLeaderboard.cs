﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

/*
 This class is used to set the UITableView to the appropriate data. This class is
 used for the world and personal leadboards as they have three columns of data.
 */
namespace throwdown.iOS
{
    public class TableSourceLeaderboard : UITableViewSource
    {

        List<string[]> TableItems;


        public TableSourceLeaderboard(List<string[]>  items)
        {
            TableItems = items;

        }

        //method from UITableViewSource that must be overriden
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return TableItems.Count;
        }

        //method from UITableViewSource that must be overriden. Called when each cell is being set
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            // checking to see if there is a cell that can be reused.
            var cell = tableView.DequeueReusableCell(MultiColumnCell.ID) as MultiColumnCell;
            // if cell has not been used create new cell
            if (cell == null)
            {
                cell = new MultiColumnCell();
            }
            // set the values in the cell
            cell.SelectedBackgroundView = new UIView { BackgroundColor = UIColor.Blue };
            cell.LeftValue.Text = TableItems[indexPath.Row][0];
            cell.MiddleValue.Text = TableItems[indexPath.Row][1];
            cell.RightValue.Text = TableItems[indexPath.Row][2];
            return cell;
        }

    }
}
