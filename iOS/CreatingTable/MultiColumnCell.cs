﻿using System.Drawing;
using UIKit;

/*
 This class is used to create 3 columns for a row for the personal
 and world leaderboard tables
 */
namespace throwdown.iOS
{
    public class MultiColumnCell : UITableViewCell
    {
        public static readonly string ID = "MultiColumnCell";

        public UILabel LeftValue { get; set; }
        public UILabel MiddleValue { get; set; }
        public UILabel RightValue { get; set; }

        public MultiColumnCell()
        {
            // right, middle and left cell based on height width and postion
            LeftValue = new UILabel(new RectangleF(0, 0, 150, 50));
            MiddleValue = new UILabel(new RectangleF(150, 0, 175, 50));
            RightValue = new UILabel(new RectangleF(325, 0, 125, 50));

            //set the cells to have the same color as the background
            LeftValue.BackgroundColor = UIColor.FromRGB(88, 142, 200);
            MiddleValue.BackgroundColor = UIColor.FromRGB(88, 142, 200);
            RightValue.BackgroundColor = UIColor.FromRGB(88, 142, 200);

            //add cells to the view
            AddSubview(LeftValue);
            AddSubview(MiddleValue);
            AddSubview(RightValue);
        }

    }
}



