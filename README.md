### What is this repository for? ###

This repository contains the Android and iOS software for the game called "Throwdown".
Throwdown is a game where you throw your phone and based on how fast you throw your phone (the faster the better) you get a score.
Scores are uploaded to the Throwdown server, and stored in a database.
The game has leaderboards so you can see how you rank in comparison with other players.

Please visit https://uocis.assembla.com/spaces/cis422w18-team7/wiki/Throwdown for more information.

### How do I get set up? ###
Install files are located in the downloads section
of the repository: https://bitbucket.org/thebravedave/throwdowngame/downloads/

### Android Setup

To install the Android version of the game, obtain the apk file from the downloads page ( https://bitbucket.org/thebravedave/throwdowngame/downloads/ )
and follow these instructions on how to install an apk file for an Android device:
https://www.wikihow.tech/Install-APK-Files-on-Android



### iOS Setup

iOS Installation Instructions:

1) Obtain UUID of iPhone - According to "https://bjango.com/help/iphoneudid/" it can be done in the following steps:
Open iTunes (the Mac or PC app, not iTunes on your iPhone).
Plug in your iPhone, iPod touch or iPad.
Click its name under the devices list.
Ensure you�re on the Summary tab.
Click on the text that says Serial Number. It should change to say Identifier (UDID).
Select Copy from the Edit menu.
Your UDID is now in the clipboard, so you can paste it into an email or message.

2) email anisha.malynur@gmail.com your iPhone's UUID

3) Wait until you recieve an email back containing a .ipa file.

4)  According to https://docs.microsoft.com/en-us/xamarin/ios/deploy-test/app-distribution/ipa-support?tabs=vsmac"
"The resulting IPA Package can be delivered to your test users for installing on their iOS devices or shipped for Enterprise deployment. 
No matter which method is chosen, the end user will install the package in their iTunes application on their Mac or Windows PC by double-clicking 
the IPA file (or dragging it onto the open iTunes window). The new iOS application will be shown in the My Apps section, where you can right-click 
on it and get information about the application"